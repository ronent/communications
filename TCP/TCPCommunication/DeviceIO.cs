﻿using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TCPCommunication
{
    public class DeviceIO : IDeviceIO
    {
        #region Fields
        public const int GENERIC_REPORT_SIZE = 512;

        private TcpClient socket;
        private CommunicationManager communicationManager;
        private volatile bool checkConnection = true;
        private PingChecker pingChecker;
        private SslStream sslStream;
        private string id;
        private string shortId;

        #endregion
        #region Constructor
        public DeviceIO(TcpClient tcpClient, SslStream sslStream, CommunicationManager communicationManager)
        {
            IsDisposed = false;
            this.socket = tcpClient;
            this.sslStream = sslStream;

            this.communicationManager = communicationManager;
            pingChecker = new PingChecker(ShortId);

            waitForData();
        }

        #endregion
        #region Methods
        private void waitForData()
        {
            byte[] resp = new byte[GENERIC_REPORT_SIZE];

            pingChecker.OnTCPDisconnected += () =>
                {
                    removeSocket();
                };

            pingChecker.Start();

            Task task = new Task(() =>
            {
                while (checkConnection)
                {
                    if (sslStream.CanRead)
                    {
                        if (socket.Client != null && socket.Client.Poll(0, SelectMode.SelectRead))
                        {
                            byte[] checkConn = new byte[1];
                            if (socket.Client != null && socket.Client.Receive(checkConn, SocketFlags.Peek) == 0)
                            {
                                removeSocket();
                                break;
                            }
                        }
                    }

                    Task.Delay(10);
                }
            });

            Task.Factory.StartNew(async () =>
                {
                    try
                    {
                        //loop till can't read from the socket
                        //this will holds the connectivity to the client
                        while (sslStream.CanRead)
                        {
                            int bytescount = await sslStream.ReadAsync(resp, 0, GENERIC_REPORT_SIZE);
                            if (bytescount > 0 && sslStream.CanWrite)
                                invokeOnDataReceived(Utilities.SubArray(resp, 0, bytescount));
                            else
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (!ex.Message.Contains("Unable to read data from the transport connection") &&
                            !ex.Message.Contains("Cannot access a disposed object.") &&
                            !ex.Message.Contains("The read operation failed"))
                        {
                            Log.Instance.WriteError("Fail in WaitForData", this, ex);
                        }
                    }
                }, TaskCreationOptions.LongRunning);
        }

        private void removeSocket()
        {
            checkConnection = false;
            communicationManager.Remove(ShortId);
        }

        #endregion
        #region IDeviceIO
        public event DataReceivedEventHandler OnDataReceived;
        public event DataSentEventHandler OnDataSent;
        public Action OnOffline { get; set; }

        public bool IsDisposed { get; private set; }

        public string Id
        {
            get
            {
                if (id == null)
                    id = socket.Client.RemoteEndPoint.ToString();

                return id;
            }
        }

        public string ShortId
        {
            get
            {
                if (shortId == null)
                    shortId = ((System.Net.IPEndPoint)(socket.Client.RemoteEndPoint)).Address.ToString();

                return shortId;
            }
        }

        public async void SendData(byte[] data)
        {
            await sslStream.WriteAsync(data, 0, data.Length);            
            invokeOnDataSent(data);
        }

        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                checkConnection = false;
                socket.Close();
                sslStream.Close();
                socket = null;

                if (OnOffline != null)
                    OnOffline();
            }
        }

        #endregion
        #region Invoke Methods
        private void invokeOnDataReceived(byte[] data)
        {
            Utilities.InvokeEvent(OnDataReceived, this, new DataTransmittedEventArgs(data));
        }

        private void invokeOnDataSent(byte[] data)
        {
            Utilities.InvokeEvent(OnDataSent, this, new DataTransmittedEventArgs(data));
        }

        #endregion
    }
}
