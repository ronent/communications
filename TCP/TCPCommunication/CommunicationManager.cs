﻿using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.Modules;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TCPCommunication
{
    [Export(typeof(ICommunicationManager)),]
    [ExportMetadata("Name", "TCP Communication")]
    public class CommunicationManager : ICommunicationManager, IDisposable
    {
        #region Members
        private static X509Certificate serverCertificate = X509Certificate.CreateFromCertFile("fourtec-simio.cer"); 

        private ConcurrentDictionary<string, DeviceIO> connectedDevices;
        private volatile bool scanForDevices;
        private Int16 port;

        #endregion
        #region Constructor
        public CommunicationManager()
        {
            connectedDevices = new ConcurrentDictionary<string, DeviceIO>();

            try
            {
                port = Convert.ToInt16(ConfigurationManager.AppSettings["TCPListenerPort"]);
            }
            catch
            {
                throw new IOException("Error reading 'TCPListenerPort' from configuration file.");
            }
        }

        #endregion
        #region ICommunicationManager
        public event DeviceConnectedDelegate OnDeviceConnected;
        public event DeviceRemovedDelegate OnDeviceRemoved;

        public void ScanForDevices()
        {
            Log.Instance.Write("[TCP Communication] ==> Started scanning for devices on port: '{0}'", this, Log.LogSeverity.DEBUG, port);

            scanForDevices = true;
            start();
        }

        public void Shutdown()
        {
            Dispose();
        }

        public void Dispose()
        {
            scanForDevices = false;
        }

        #endregion
        #region Methods
        private void start()
        {
            IPAddress localIP = IPAddress.Any;
            TcpListener listener = new TcpListener(localIP, port);

            listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            listener.Start();

            Task.Factory.StartNew(async () =>
            {
                while (scanForDevices)
                {
                    try
                    {
                        TcpClient tcpClient = await listener.AcceptTcpClientAsync();
                        var sslStream = setSocketProperties(tcpClient);
                        if (sslStream != null)
                            add(tcpClient, sslStream);
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.WriteError("Fail in StartListen",this, ex);
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        private SslStream setSocketProperties(TcpClient client)
        {
            client.NoDelay = true;
            client.Client.NoDelay = true;
            client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);

            try
            {
                SslStream sslStream = new SslStream(client.GetStream(), false);

                sslStream.AuthenticateAsServer(serverCertificate, false, SslProtocols.Tls, true);
                return sslStream;
            }
            catch (Exception ex) 
            {
                Log.Instance.WriteError("On setSocketProperties()", this, ex);
            }

            return null;
        }

        private void add(TcpClient client, SslStream sslStream)
        {
            DeviceIO deviceIO = new DeviceIO(client, sslStream, this);
            connectedDevices.AddOrUpdate(((System.Net.IPEndPoint)(client.Client.RemoteEndPoint)).Address.ToString(), deviceIO, (key, value) => deviceIO);

            Utilities.InvokeEvent(OnDeviceConnected, this, new ConnectionEventArgs() { DeviceIO = deviceIO, UID = deviceIO.Id });
        }

        internal void Remove(string ipAddress)
        {
            DeviceIO device;
            if (connectedDevices.TryRemove(ipAddress, out device))
            {
                Utilities.InvokeEvent(OnDeviceRemoved, this, new ConnectionEventArgs() { DeviceIO = device, UID = device.Id });
                device.Dispose();
            }
        }

        #endregion
    }
}