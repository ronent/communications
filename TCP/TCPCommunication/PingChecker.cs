﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TCPCommunication
{
    internal class PingChecker
    {
        public Action OnTCPDisconnected;

        private readonly int pingDelay = 1000 * 15;
        private static readonly int pingTimeout = 500;

        private volatile bool run = false;
        public string Host { get; private set; }

        public PingChecker(string host)
        {
            Host = host;
        }

        public void Start()
        {
            run = true;
            Task.Factory.StartNew(() =>
                {
                    while (run)
                    {
                        if (!PingHost(Host))
                        {
                            if (OnTCPDisconnected != null)
                                OnTCPDisconnected();
                        }

                        Task.Delay(pingDelay).Wait();
                    }
                }, TaskCreationOptions.AttachedToParent);
        }

        public void Stop()
        {
            run = false;
        }

        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();

            try
            {
                int retries = 3;
                while (--retries > 0)
                {
                    PingReply reply = pinger.Send(nameOrAddress, pingTimeout);
                    pingable = reply.Status == IPStatus.Success;

                    if (pingable)
                        break;
                }
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }

            return pingable;
        }
    }
}
