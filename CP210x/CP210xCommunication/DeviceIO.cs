﻿using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Extentions;
using System.Threading;
using Log4Tech;

namespace CP210xCommunication
{
    public class DeviceIO : ISerialDeviceIO
    {
        #region Fields
        private bool useLog = false;

        private CommunicationManager parent;
        private SerialPort port;
        private string id;

        #endregion
        #region Events
        public event DataReceivedEventHandler OnDataReceived;
        public event DataSentEventHandler OnDataSent;
        public Action OnOffline { get; set; }

        #endregion
        #region Constructor
        public DeviceIO(CommunicationManager parent, SerialPort port)
        {
            IsDisposed = false;

            this.parent = parent;
            this.port = port;
        }

        ~DeviceIO()
        {
            Dispose();
        }

        #endregion
        #region Properties
        public string ShortId
        {
            get { return Id; }
        }

        public string Id
        {
            get
            {
                if (id == null)
                    id = port.PortName;

                return id;
            }
        }

        public bool IsDisposed { get; private set; }

        public string NewLine { get { return port.NewLine; } private set { port.NewLine = value; } }

        public Encoding Encoding { get { return port.Encoding; } private set { port.Encoding = value; } }

        public int BaudRate { get { return port.BaudRate; } private set { port.BaudRate = value; } }

        #endregion
        #region Methods
        private void listenToData()
        {
            Task.Factory.StartNew(() =>
            {
                while (!IsDisposed)
                {
                    try
                    {
                        if (!port.IsOpen)
                            break;

                        string msg = port.ReadLine();
                        var e = new DataTransmittedEventArgs(port.Encoding.GetBytes(msg));

                        if (useLog)
                            Log.Instance.Write("[CP210x Communication] ==> Device '{0}' received opcode '{1}' from device, Thread #{2}.", this, Log.LogSeverity.DEBUG, ShortId, e.Data[7].ToString("X2"), Thread.CurrentThread.ManagedThreadId);

                        Utilities.InvokeEvent(OnDataReceived, this, e);
                    }
                    catch { }
                }
            }, TaskCreationOptions.LongRunning);
        }

        public void SendData(byte[] data)
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);

            port.DiscardOutBuffer();
            port.Write(data, 0, data.Length);

            if (useLog)
                Log.Instance.Write("[CP210x Communication] ==> Device '{0}' sending opcode '{1}' to device, Thread #{2}.", this, Log.LogSeverity.DEBUG, ShortId, data[7].ToString("X2"), Thread.CurrentThread.ManagedThreadId);

            Utilities.InvokeEvent(OnDataSent, this, new DataTransmittedEventArgs(data));
        }

        public void ApplyChanges(int baudRate, Encoding encoding, string newLine)
        {
            try
            {
                if (port.IsOpen)
                    port.Close();

                this.BaudRate = baudRate;
                this.Encoding = encoding;
                this.NewLine = newLine;

                port.Open();
                listenToData();
            }
            catch { }
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                parent.RemoveDevice(ShortId);

                try
                {
                    port.Close();
                    port.Dispose();

                    port = null;
                    OnDataReceived = null;
                    OnDataSent = null;
                }
                catch { }
                finally
                {
                    if (OnOffline != null)
                        OnOffline();
                }
            }
        }

        #endregion
    }
}