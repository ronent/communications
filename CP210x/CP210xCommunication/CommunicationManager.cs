﻿using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.Communication.Modules;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CP210xCommunication
{
    [Export(typeof(ICommunicationManager))]
    [ExportMetadata("Name", "CP210x Communication")]
    public class CommunicationManager : ICommunicationManager
    {
        #region Fields
        private ConcurrentDictionary<string, DeviceIO> connectedDevices;
        private ManagementEventWatcher managementEventWatcherAdd;
        private ManagementEventWatcher managementEventWatcherRemove;

        #endregion
        #region Events
        public event DeviceConnectedDelegate OnDeviceConnected;
        public event DeviceRemovedDelegate OnDeviceRemoved;

        #endregion
        #region Constructor
        public CommunicationManager()
        {
            connectedDevices = new ConcurrentDictionary<string, DeviceIO>();
        }

        #endregion
        #region Methods
        public void ScanForDevices()
        {
            addRemoveUSBHandler();
            addInsertUSBHandler();

            Log.Instance.Write("[CP210x Communication] ==> Started scanning for devices", this, Log.LogSeverity.DEBUG);

            scanExistingComPort();
        }

        public void Shutdown()
        {
            Dispose();
        }

        #endregion
        #region Private methods
        private void scanExistingComPort()
        {
            try
            {
                string query = "SELECT * FROM MSSerial_PortName";
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\WMI", query);

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    //If the serial port's instance name contains USB 
                    //it must be a USB to serial device
                    //if (queryObj["InstanceName"].ToString().Contains("USB"))
                    {
                        string port = queryObj["PortName"].ToString();
                        addNewDevice(port, queryObj["InstanceName"].ToString());
                    }
                }
            }
            catch (ManagementException e)
            {
            }
            catch (Exception ex)
            {
            }
        }

        private void addRemoveUSBHandler()
        {
            WqlEventQuery wqlEventQuery;
            ManagementScope managementScope = new ManagementScope("root\\CIMV2");
            managementScope.Options.EnablePrivileges = true;

            try
            {
                wqlEventQuery = new WqlEventQuery();
                wqlEventQuery.EventClassName = "__InstanceDeletionEvent";
                wqlEventQuery.WithinInterval = new TimeSpan(0, 0, 3);
                wqlEventQuery.Condition = "TargetInstance ISA 'Win32_USBControllerdevice'";
                managementEventWatcherRemove = new ManagementEventWatcher(managementScope, wqlEventQuery);
                managementEventWatcherRemove.EventArrived += USBRemoved;

                managementEventWatcherRemove.Start();
            }
            catch (Exception e)
            {
                if (managementEventWatcherRemove != null)
                {
                    managementEventWatcherRemove.Stop();
                }
            }

        }

        private void addInsertUSBHandler()
        {
            WqlEventQuery wqlEventQuery;
            ManagementScope managementScope = new ManagementScope("root\\CIMV2");
            managementScope.Options.EnablePrivileges = true;

            try
            {
                wqlEventQuery = new WqlEventQuery();
                wqlEventQuery.EventClassName = "__InstanceCreationEvent";
                wqlEventQuery.WithinInterval = new TimeSpan(0, 0, 3);
                wqlEventQuery.Condition = "TargetInstance ISA 'Win32_USBControllerdevice'";
                managementEventWatcherAdd = new ManagementEventWatcher(managementScope, wqlEventQuery);
                managementEventWatcherAdd.EventArrived += USBInserted;

                managementEventWatcherAdd.Start();
            }
            catch (Exception e)
            {
                if (managementEventWatcherAdd != null)
                {
                    managementEventWatcherAdd.Stop();
                }
            }
        }

        private void USBInserted(object sender, EventArgs e)
        {
            //get the new port and raise an event
            getNewlyPort();
        }

        private void USBRemoved(object sender, EventArgs e)
        {
            //remove from the current list
            getRemovedPort();
        }
        private string getHardwareId(string port)
        {
            try
            {
                string query = string.Format("SELECT * FROM MSSerial_PortName");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\WMI", query);

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    //If the serial port's instance name contains USB 
                    if (queryObj["PortName"].ToString() == port)
                    {
                        return queryObj["InstanceName"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return string.Empty;
        }

        private void getNewlyPort()
        {
            try
            {
                //get the current list of connected ports devices
                var temp = connectedDevices.Keys.ToList();
                //go over the current connected ports
                foreach (var port in SerialPort.GetPortNames())
                {
                    //checks if the port not in use then raise the event
                    SerialPort serialPort = new SerialPort(port);
                    if (!serialPort.IsOpen)
                    {
                        //checks if the current port exists in the list
                        if (!temp.Contains(port))
                        {
                            string hardwareId = getHardwareId(port);
                            addNewDevice(port, hardwareId);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                
            }
        }

        private void getRemovedPort()
        {
            //get the connected ports list
            var temp = SerialPort.GetPortNames().ToList();
            //go over the existing connected devices
            connectedDevices.Keys.ToList().ForEach((port) =>
                {
                    //checks if connected device exists in the list of connected ports
                    if (!temp.Exists(x => x == port))
                    {
                        //raise remove device
                        RemoveDevice(port);
                    }
                });
        }

        private void addNewDevice(string portName, string hardwareId)
        {
            try
            {
                SerialPort port = new SerialPort(portName);
                if (port.IsOpen)
                    return;

                DeviceIO outDevice;
                if (connectedDevices.TryGetValue(port.PortName, out outDevice))
                    return;
                else
                    connectedDevices.TryAdd(port.PortName, outDevice = new DeviceIO(this, port));

                ConnectionEventArgs args = new ConnectionEventArgs()
                {
                    UID = outDevice.Id,
                    DeviceIO = outDevice,
                };

                Log.Instance.Write("[CP120x Communication] ==> Device connected: '{0}'", this, Log.LogSeverity.DEBUG, args.DeviceIO.ShortId);

                Utilities.InvokeEvent(OnDeviceConnected, this, args);
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("On addNewDevice()", this, ex);
            }
        }

        internal void RemoveDevice(string comPort)
        {
            DeviceIO removedDevice;
            if (connectedDevices.TryRemove(comPort, out removedDevice))
            {
                if (removedDevice != null)
                {
                    ConnectionEventArgs args = new ConnectionEventArgs()
                    {
                        UID = removedDevice.Id,
                        DeviceIO = removedDevice,
                    };

                    Log.Instance.Write("[CP120x Communication] ==> Device disconnected: '{0}'", this, Log.LogSeverity.DEBUG, args.DeviceIO.ShortId);

                    Utilities.InvokeEvent(OnDeviceRemoved, this, args);

                    removedDevice.Dispose();
                }
            }
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            try
            {
                managementEventWatcherAdd.Stop();
                managementEventWatcherAdd.Dispose();
            }
            catch { }

            try
            {
                managementEventWatcherRemove.Stop();
                managementEventWatcherRemove.Dispose();
            }
            catch { }

            foreach (var device in connectedDevices)
            {
                device.Value.Dispose();
            }
        }

        #endregion
    }
}