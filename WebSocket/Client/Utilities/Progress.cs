﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient.Utilities
{
    public delegate void ProgressReportDelegate(object sender, int percentage);

    public class Progress
    {
        public event ProgressReportDelegate OnProgressReport;
        public List<PartialProgress> Tasks { get; private set; }

        private int lastPercentage = 0;

        public Progress()
        {
            Tasks = new List<PartialProgress>();
        }

        public void Add(PartialProgress task)
        {
            Tasks.Add(task);
            task.OnProgressReport += task_OnProgressReport;
        }

        void task_OnProgressReport(object sender, int percentage)
        {
            if (percentage > lastPercentage)
            {
                lastPercentage = percentage;
                if (OnProgressReport != null)
                    OnProgressReport.Invoke(this, percentage);
            }
        }

        public void AddRange(IEnumerable<PartialProgress> tasks)
        {
            Tasks.AddRange(tasks);

            foreach (var item in tasks)
            {
                item.OnProgressReport += task_OnProgressReport;
            }
        }

        public PartialProgress GetPartialProgress(eTasks task)
        {
            foreach (var item in Tasks)
            {
                if (item.Task == task)
                    return item;
            }

            return null;
        }
    }
}
