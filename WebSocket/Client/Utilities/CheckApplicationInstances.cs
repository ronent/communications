﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace WeboomerangClient.Utilities
{
    public delegate void InstanceChangedDelegate(bool isRunning);

    public class CheckApplicationInstances
    {
        private const int k_TimeToSleep = 500;
        private string appName;

        public InstanceChangedDelegate OnInstanceChanged;

        public CheckApplicationInstances(string appName)
        {
            this.appName = appName;
        }

        private volatile bool m_IsDataSuiteRunning;
        public bool IsDataSuiteRunning
        {
            get { return m_IsDataSuiteRunning; }
            private set
            {
                if (m_IsDataSuiteRunning != value)
                {
                    m_IsDataSuiteRunning = value;

                    if (OnInstanceChanged != null)
                        OnInstanceChanged.Invoke(value);
                }
            }
        }

        public void Start()
        {
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Process[] dataSuiteProcess = Process.GetProcessesByName(appName);

                    bool isRunning = dataSuiteProcess.Length != 0;

                    Task.Factory.StartNew(() => IsDataSuiteRunning = isRunning);

                    Thread.Sleep(k_TimeToSleep);
                }

            }, TaskCreationOptions.LongRunning);
        }
    }
}
