﻿using System;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

namespace SocketClient.Communication
{
    public delegate void InternetConnectionChangeDelegate(bool isConnected);

    public class ConnectivityCheker
    {
        public event InternetConnectionChangeDelegate OnInternetConnectionChange;

        public bool IsChecking { get; private set; }
        public bool IsConnected
        {
            get { return m_IsConnected; }
            private set
            {
                if (m_IsConnected != value)
                {
                    m_IsConnected = value;

                    if (OnInternetConnectionChange != null)
                        OnInternetConnectionChange.Invoke(m_IsConnected);
                }
            }
        }

        private bool m_IsConnected;

        public void Start()
        {
            Task.Factory.StartNew(() =>
            {
                IsChecking = true;

                while (IsChecking)
                {
                    try
                    {
                        IsConnected = NetworkInterface.GetIsNetworkAvailable();
                    }
                    catch { }
                    finally
                    {
                        Thread.Sleep(1000);
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        public void Stop()
        {
            IsChecking = false;
        }
    }
}
