﻿using SocketProtocol.Parser;
using SuperSocket.ClientEngine;
using System;
using System.Threading;
using System.Threading.Tasks;
using WebSocket4Net;
using System.Configuration;
using WeboomerangClient.Utilities;
using Log;
using System.Diagnostics;

namespace SocketClient.Communication
{
    public delegate void ConnectionStatusChangedDelegate(eSocketConnectionState status);
    public delegate void DataReceivedDelegate(object sender, WebSocket4Net.DataReceivedEventArgs e);

    public enum eSocketConnectionState
    {
        Disconnected = 0,
        Connecting = 1,
        Connected = 2,
    }

    public class WebSocketManager : ILog
    {
        #region Events
        public event MessageLoggedDelegate OnMessageLogged;

        public event ConnectionStatusChangedDelegate OnConnectionStateChanged;
        public event DataReceivedDelegate OnDataReceived;

        #endregion
        #region Fields
        private WebSocket websocket;
        private volatile eSocketConnectionState m_Status;

        #endregion
        #region Members
        public eSocketConnectionState State
        {
            get { return m_Status; }
            set
            {
                if (m_Status != value)
                {
                    m_Status = value;
                    InvokeOnLog(string.Format("Socket Client => Status changed to: {0}", m_Status));

                    if (OnConnectionStateChanged != null)
                        OnConnectionStateChanged.Invoke(m_Status);
                }
            }
        }

        private WebSocketState socketState
        {
            get
            {
                try
                {
                    return websocket.State;
                }
                catch
                {
                    return WebSocketState.None;
                }
            }
        }

        public bool IsConnected
        {
            get
            {
                if (websocket == null)
                    return false;

                return websocket.State == WebSocketState.Open;
            }
        }

        private string uri;
        public string Uri
        {
            get
            {
                if (uri == null)
                    return "ws://" + ConfigurationManager.AppSettings["serverUri"];
                else
                    return uri;
            }
            private set
            {
                uri = value;
            }
        }

        #endregion
        #region Constructor
        public WebSocketManager()
        {
            m_Status = eSocketConnectionState.Disconnected;
        }

        public WebSocketManager(string uri)
            :this()
        {
            if (uri != null)
                Uri = "ws://" + uri;
        }

        #endregion
        #region Methods
        public void Start()
        {
            if (socketState == WebSocketState.Open || socketState == WebSocketState.Connecting)
                return;

            try
            {
                initialize();
                websocket.Open();
            }
            catch (Exception ex)
            {
                if (ex.Message != "The socket is connecting, cannot connect again!"
                    && ex.Message != "The socket is connected, you neednt' connect again!")
                {
                    InvokeOnLog(string.Format("Socket Client => On Start()"), ex);
                    State = eSocketConnectionState.Disconnected;
                }
            }
        }

        public void Stop()
        {
            if (socketState != WebSocketState.Open)
                return;

            try
            {
                if (websocket != null)
                {
                    websocket.Closed -= new EventHandler(websocket_Closed);
                    websocket.Close();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != "The socket is not connected!")
                {
                    InvokeOnLog("Socket Client => On stop: ", ex);
                }
            }
            finally
            {
                State = eSocketConnectionState.Disconnected;
            }
        }

        private void initialize()
        {
            State = eSocketConnectionState.Connecting;

            websocket = new WebSocket(Uri);
            InvokeOnLog(string.Format("Socket Client => Connecting to {0}.", Uri));

            websocket.AllowUnstrustedCertificate = true;

            websocket.Opened += new EventHandler(websocket_Opened);
            websocket.Error += new EventHandler<ErrorEventArgs>(websocket_Error);
            websocket.Closed += new EventHandler(websocket_Closed);
            websocket.DataReceived += new EventHandler<WebSocket4Net.DataReceivedEventArgs>(websocket_DataReceived);
            websocket.EnableAutoSendPing = true;
        }

        public void InvokeOnLog(string msg)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, msg);
        }

        public void InvokeOnLog(string msg, Exception ex)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, string.Format("{0}, {1}", msg, ex));
        }

        #endregion
        #region WebSocket
        internal void Send(byte[] data)
        {
            Task.Factory.StartNew(() => 
            {
                try
                {
                    websocket.Send(data, 0, data.Length);
                }
                catch (Exception ex)
                {
                    InvokeOnLog(string.Format("On Send"), ex); 
                }
            });
        }

        private void websocket_DataReceived(object sender, WebSocket4Net.DataReceivedEventArgs e)
        {
            if (OnDataReceived != null)
                OnDataReceived(sender, e);
        }

        private void websocket_Closed(object sender, EventArgs e)
        {
            handleDisconnection();
        }

        object locker = new object();
        private void handleDisconnection()
        {
            lock (locker)
            {
                try
                {
                    websocket.Close();
                }
                catch { }
                finally
                {
                    Thread.Sleep(100);
                }

                State = eSocketConnectionState.Disconnected;
            }
        }

        private void websocket_Error(object sender, ErrorEventArgs e)
        {
            if (e.Exception.Message != "No connection could be made because the target machine actively refused it"
                && e.Exception.Message != "You must send data by websocket after websocket is opened!"
                && e.Exception.Message != "An existing connection was forcibly closed by the remote host"
                && e.Exception.Message != "No such host is known"
                && e.Exception.Message != "A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond")
            {
                InvokeOnLog(string.Format("On websocket_Error"), e.Exception);
            }

            handleDisconnection();
        }

        private void websocket_Opened(object sender, EventArgs e)
        {
            State = eSocketConnectionState.Connected;
        }

        #endregion
    }
}
