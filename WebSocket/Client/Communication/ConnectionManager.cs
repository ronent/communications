﻿using Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeboomerangClient.Utilities;

namespace SocketClient.Communication
{
    public delegate void EnabledChangedDelegate(bool isEnable);

    public class ConnectionManager : ILog
    {
        #region Events
        public event MessageLoggedDelegate OnMessageLogged;
        public event EnabledChangedDelegate OnEnableChanged;

        public event InstanceChangedDelegate OnDataSuiteInstanceChanged
        {
            add { dataSuiteInstanceChecker.OnInstanceChanged += value; }
            remove { dataSuiteInstanceChecker.OnInstanceChanged -= value; }
        }

        public event ConnectionStatusChangedDelegate OnConnectionStateChanged
        {
            add { WebSocket.OnConnectionStateChanged += value; }
            remove { WebSocket.OnConnectionStateChanged -= value; }
        }

        #endregion
        #region Fields
        public static WebSocketManager WebSocket { get; private set; }
        private ConnectivityCheker connectivityChecker;
        private CheckApplicationInstances dataSuiteInstanceChecker;

        private volatile bool started;
        private volatile bool isEnabled;
        private object startLocker = new object();

        #endregion
        #region Constructor
        public ConnectionManager(string serverURI = null)
        {
            ServerUri = serverURI;

            initialize();
            initializeEvents();
        }

        private void initialize()
        {
            WebSocket = new WebSocketManager(ServerUri);
 	        connectivityChecker = new ConnectivityCheker();
            dataSuiteInstanceChecker = new CheckApplicationInstances("DataSuite");

            IsDataSuiteRunning = false;
            IsInternetConnected = false;
            Started = false;
        }

        private void initializeEvents()
        {
            connectivityChecker.OnInternetConnectionChange += (isConnected) =>
                {
                    IsInternetConnected = isConnected;
                    internetConnectionChanged(isConnected); 
                };

            dataSuiteInstanceChecker.OnInstanceChanged += (isConnected) =>
                {
                    IsDataSuiteRunning = isConnected;
                    dataSuiteInstanceChanged(isConnected);
                };

            WebSocket.OnConnectionStateChanged += (state) =>
                {
                    switch (state)
                    {
                        case eSocketConnectionState.Disconnected:
                            started = false;
                            Start();
                            break;
                        case eSocketConnectionState.Connecting:
                            break;
                        case eSocketConnectionState.Connected:
                            break;
                        default:
                            break;
                    }
                };

            if (WebSocket is ILog)
            {
                (WebSocket as ILog).OnMessageLogged += (sender, msg) =>
                    {
                        InvokeOnLog(msg);
                    };
            }

            connectivityChecker.Start();
            dataSuiteInstanceChecker.Start();
        }

        #endregion
        #region Properties
        public string ServerUri { get; private set; }
        public bool Started { get { return started; } private set { started = value; } }
        public bool IsInternetConnected { get; private set; }
        public bool IsDataSuiteRunning { get; private set; }
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                enableDisablenChanged(value);
            }
        }

        public eSocketConnectionState WebSocketState { get { return WebSocket.State; } }

        #endregion
        #region Methods
        public void Start()
        {
            if (Started)
                return;

            lock (startLocker)
            {
                if (!IsDataSuiteRunning && IsInternetConnected && IsEnabled && WebSocket.State != eSocketConnectionState.Connecting)
                {
                    Started = true;
                    WebSocket.Start();
                }
            }
        }

        public void Stop()
        {
            if (!Started)
                return;
            else
            {
                Started = false;
                WebSocket.Stop();
            }
        }

        public void InvokeOnLog(string msg)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, msg);
        }

        public void InvokeOnLog(string msg, Exception ex)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, string.Format("{0}, {1}", msg, ex));
        }

        #endregion
        #region Private Methods
        private void dataSuiteInstanceChanged(bool isConnected)
        {
            if (!isConnected)
            {
                InvokeOnLog("Socket Client => DataSuite is closed.");
                Start();
            }
            else
            {
                InvokeOnLog("Socket Client => DataSuite is open.");
                Stop();
            }
        }

        private void internetConnectionChanged(bool isConnected)
        {
            if (isConnected)
            {
                InvokeOnLog("Socket Client => Internet connection is up.");
                Start();
            }
            else
            {
                InvokeOnLog("Socket Client => Internet connection is down.");
                Stop();
            }
        }

        private void enableDisablenChanged(bool isEnabled)
        {
            if (isEnabled)
            {
                InvokeOnLog("Socket Client => Connection is now enabled.");
                Start();
            }
            else
            {
                InvokeOnLog("Socket Client => Connection is now disabled.");
                Stop();
            }

            if (OnEnableChanged != null)
                OnEnableChanged(isEnabled);
        }

        #endregion
    }
}
