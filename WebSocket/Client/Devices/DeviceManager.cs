﻿using Infrastructure.Communication;
using Infrastructure.Communication.Modules.HID;
using Log;
using SocketClient.Communication;
using SocketProtocol.Messages;
using SocketProtocol.Messages.Arguments;
using SocketProtocol.Parser;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using WeboomerangClient.Utilities;

namespace SocketClient.Devices
{
    public delegate void DeviceConnectionDelegate(DeviceConnection.eStatus status, DeviceIO device);

    public class DeviceManager : ILog
    {
        #region Events
        public event MessageLoggedDelegate OnMessageLogged;
        public event DeviceConnectionDelegate OnDeviceConnection;

        public event ConnectionStatusChangedDelegate OnConnectionStateChanged 
        {
            add { ConnectionManager.OnConnectionStateChanged += value; }
            remove { ConnectionManager.OnConnectionStateChanged -= value; }
        }

        public event InstanceChangedDelegate OnDataSuiteInstanceChanged
        {
            add { ConnectionManager.OnDataSuiteInstanceChanged += value; }
            remove { ConnectionManager.OnDataSuiteInstanceChanged -= value; }
        }

        public event EnabledChangedDelegate OnEnableChanged
        {
            add { ConnectionManager.OnEnableChanged += value; }
            remove { ConnectionManager.OnEnableChanged -= value; }
        }

        #endregion
        #region Fields
        protected static Parser parser;
        public static ConnectionManager ConnectionManager;
        private static IHIDCommunicationManager hidCommunicationManager;

        protected ConcurrentDictionary<string, DeviceIO> devices = new ConcurrentDictionary<string, DeviceIO>();

        #endregion
        #region Constructor
        public DeviceManager()
        {
            ConnectionManager = new ConnectionManager();
            initialize();
        }

        public DeviceManager(string uri)
        {
            ConnectionManager = new ConnectionManager(uri);
            initialize();
        }

        private void initialize()
        {
            IsEnabled = true;
            InvokeOnLog("Socket Client => DeviceManager has started.");

            hidCommunicationManager = new HidCommunication.CommunicationManager();

            InitializeParser();
            initializeParserEvents();
            subscribeWebSocketManagerEvents();
        }

        protected virtual void InitializeParser()
        {
            parser = new Parser();
        }

        private void initializeParserEvents()
        {
            InitializeParserEvents();

            parser.OnLogMessage += (message) =>
                {
                    InvokeOnLog(message);
                };

            parser.OnSendMessage += (data) =>
                {
                    try
                    {
                        ConnectionManager.WebSocket.Send(data);
                    }
                    catch (Exception ex)
                    {
                        InvokeOnLog("Socket Client => On OnSendMessage: ", ex);
                    }
                };

            parser.DeviceIDsCommand.OnDeviceIds += (message) =>
                {
                    var deviceIds = message.DeviceIds;
                    if (deviceIds != null && deviceIds.Count != 0)
                    {
                        InvokeOnLog(string.Format("Socket Client => Registered DeviceIDs: {0}", string.Join<DeviceID>(", ", deviceIds)));
                        hidCommunicationManager.RegisterDeviceIds(deviceIds);
                        hidCommunicationManager.ScanForDevices();
                    }
                    else
                    {
                        InvokeOnLog(string.Format("Socket Client => Device Ids is empty"));
                    }
                };

            parser.PacketCommand.OnPacket += (message) =>
                {
                    devices[message.DevicePath].SendData(message.Data);
                };

            parser.BundleCommand.OnBundle += (message) =>
                {
                    devices[message.DevicePath].InvokeLoop(message.LoopArguments);
                };

            parser.IgnoreOpCodesCommand.OnIgnoreOpCodes += (message) =>
                {
                    devices[message.DevicePath].SetIgnoreOpCodes(message.OpCodeStartByte, message.OpCodesToIgnore);
                };

        }

        protected virtual void InitializeParserEvents() { }

        private void subscribeWebSocketManagerEvents()
        {
            ConnectionManager.OnConnectionStateChanged += (status) =>
            {
                switch (status)
                {
                    case eSocketConnectionState.Connected:
                        Thread.Sleep(100);
                        SendMessageToServer(new DeviceIDs());
                        subscribeCommunicationChannelsEvents();
                        break;
                    case eSocketConnectionState.Connecting:
                    case eSocketConnectionState.Disconnected:
                        removeAllDevices();
                        unscribeCommunicationChannelsEvents();
                        hidCommunicationManager.Shutdown();
                        break;
                    default:
                        break;
                }
            };

            ConnectionManager.WebSocket.OnDataReceived += (s, e) =>
                {
                    if (e.Data[1] == 0)
                    {
                        InvokeOnLog("Socket Client => websocket_DataReceived, data is empty");
                        return;
                    }

                    parser.Parse(e.Data);
                };

            if (ConnectionManager is ILog)
            {
                (ConnectionManager as ILog).OnMessageLogged += (sender, msg) =>
                    {
                        InvokeOnLog(msg);
                    };
            }
        }

        #endregion
        #region Properties
        public bool DataSuiteInstanceRunning { get { return ConnectionManager.IsDataSuiteRunning; } }
        public bool IsEnabled { get { return ConnectionManager.IsEnabled; } set { ConnectionManager.IsEnabled = value; } }

        #endregion
        #region HID Communication Manager
        private void unscribeCommunicationChannelsEvents()
        {
            lock (subscribeLocker)
            {
                hidCommunicationManager.OnDeviceConnected -= hidCommunicationManager_OnDeviceConnected;
                hidCommunicationManager.OnDeviceRemoved -= hidCommunicationManager_OnDeviceRemoved;

                subscribed = false;
            }
        }

        private bool subscribed = false;
        private object subscribeLocker = new object();
        private void subscribeCommunicationChannelsEvents()
        {
            lock (subscribeLocker)
            {
                if (subscribed)
                    return;

                hidCommunicationManager.OnDeviceConnected += hidCommunicationManager_OnDeviceConnected;
                hidCommunicationManager.OnDeviceRemoved += hidCommunicationManager_OnDeviceRemoved;

                subscribed = true;
            }
        }

        void hidCommunicationManager_OnDeviceRemoved(object sender, ConnectionEventArgs e)
        {
            try
            {
                DeviceIO removedDevice;
                if (devices.TryRemove(e.UID, out removedDevice))
                {
                    InvokeOnLog(string.Format("Socket Client => Device removed '{0}'.", e.DeviceIO.ShortId));
                    removedDevice.Close();

                    deviceConnection(removedDevice, DeviceConnection.eStatus.Disconnected);
                }
            }
            catch (Exception ex)
            {
                InvokeOnLog("Socket Client => On RemoveDevice", ex);
            }
        }

        private void deviceConnection(DeviceIO device, DeviceConnection.eStatus connection)
        {
            if (OnDeviceConnection != null)
                OnDeviceConnection(connection, device);

            DeviceConnection msg = new DeviceConnection()
            {
                Status = connection,
                DeviceID = device.DeviceId,
                DevicePath = device.Id,
            };

            SendMessageToServer(msg);
        }

        void hidCommunicationManager_OnDeviceConnected(object sender, ConnectionEventArgs e)
        {
            try
            {
                ConnectionManager.WebSocket.InvokeOnLog(string.Format("Socket Client => Found new device '{0}'.", e.DeviceIO.ShortId));

                DeviceIO newDevice = GenerateNewDeviceIO(e as HIDConnectionEventArgs);
                var device = devices.AddOrUpdate(newDevice.Id, newDevice, (key, oldValue) => newDevice);

                deviceConnection(device, DeviceConnection.eStatus.Connected);
            }
            catch (Exception ex)
            {
                ConnectionManager.WebSocket.InvokeOnLog("Socket Client => On AddDevice", ex);
            }
        }

        protected virtual DeviceIO GenerateNewDeviceIO(HIDConnectionEventArgs args)
        {
            return new DeviceIO(this, args);
        }

        private void removeAllDevices()
        {
            foreach (var item in devices)
            {
                hidCommunicationManager_OnDeviceRemoved(this, new HIDConnectionEventArgs()
                {
                    DeviceID = item.Value.DeviceId,
                    DeviceIO = item.Value.DeviceIo,
                    UID = item.Key,
                });
            }
        }

        #endregion
        #region Methods
        public void InvokeOnLog(string msg)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, msg);
        }

        public void InvokeOnLog(string msg, Exception ex)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, string.Format("{0}, {1}", msg, ex));
        }

        internal void SendMessageToServer(BaseMessage message)
        {
            Task.Factory.StartNew(() =>
                parser.SendMessage(message));
        }

        public eSocketConnectionState State
        {
            get { return ConnectionManager.WebSocketState; }
        }

        public void Start()
        {
            ConnectionManager.Start();
        }

        public void Stop()
        {
            ConnectionManager.Stop();
        }

        #endregion
    }
}
