﻿using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using SocketClient.Devices.Optimizations;
using SocketClient.Utilities;
using SocketProtocol.Messages;
using SocketProtocol.Messages.Arguments;
using SocketProtocol.Parser;
using System;
using System.Collections.Generic;

namespace SocketClient.Devices
{
    public class DeviceIO
    {
        #region Events
        public event ProgressReportDelegate OnBundleProgressReport;

        #endregion
        #region Fields
        public Action OnDeviceClose;

        public DeviceID DeviceId { get; private set; }
        public IDeviceIO DeviceIo { get; private set; }
        public string Id { get; private set; }
        public IgnoredOpCodes IgnoredOpCodes { get; private set; }

        private bool funnel = false;
        protected DeviceManager parent;

        #endregion
        #region Constructor
        public DeviceIO(DeviceManager deviceManager, HIDConnectionEventArgs e)
        {
            parent = deviceManager;

            DeviceId = e.DeviceID;
            DeviceIo = e.DeviceIO;
            Id = e.UID;
            
            initialize();
            registerIOEvents();
        }

        #endregion
        #region Methods
        private void registerIOEvents()
        {
            funnel = true;
            DeviceIo.OnDataReceived += deviceIO_OnDataReceived;
        }

        private void unregisterIOEvents()
        {
            funnel = false;
            DeviceIo.OnDataReceived -= deviceIO_OnDataReceived;
        }

        private void initialize()
        {
            IgnoredOpCodes = new Optimizations.IgnoredOpCodes();
        }

        #endregion
        #region IO Methods
        void deviceIO_OnDataReceived(object sender, DataTransmittedEventArgs e)
        {
            try
            {
                if (funnel && !IgnoredOpCodes.Contains(e.Data) && e.Data[1] != 0)
                {
                    //Log.Log.WriteOutgoing("Socket Client", DeviceName, e.Data, false);

                    parent.SendMessageToServer(new Packet()
                    {
                        Data = e.Data,
                        DevicePath = Id,
                    });
                }
            }
            catch (Exception ex)
            {
                parent.InvokeOnLog("Socket Client => On deviceIO_OnDataReceived", ex);
            }
        }

        internal void SendData(byte[] data)
        {
            //Log.Log.WriteIncoming("Socket Client", DeviceName, data, false);
            DeviceIo.SendData(data);
        }

        #endregion
        #region Methods
        internal void Close()
        {
            if (OnDeviceClose != null)
                OnDeviceClose();

            DeviceIo.Dispose();
        }

        internal void SetIgnoreOpCodes(int opCodeStartByte, IEnumerable<byte[]> opCodesToIgnore)
        {
            IgnoredOpCodes.OpCodes = opCodesToIgnore;
            IgnoredOpCodes.StartByte = opCodeStartByte;
        }

        internal void InvokeLoop(LoopArgs args)
        {
            unregisterIOEvents();
            LoopThroughAsLongAggregate loop = new LoopThroughAsLongAggregate(this, args);

            loop.OnProgressReport += (sender, percent) =>
            {
                parent.SendMessageToServer(new BundleProgress()
                {
                    DevicePath = Id,
                    Percents = percent,
                });

                bundleProgressReport(percent);
            };

            loop.OnFinishedLoopThrough += (sender, e) =>
            {
                parent.SendMessageToServer(new SocketProtocol.Messages.Arguments.Bundle()
                {
                    DevicePath = Id,
                    Packets = e.Packets,
                });

                registerIOEvents();
            };

            loop.Start();
        }

        private void bundleProgressReport(int percents)
        {
            if (OnBundleProgressReport != null)
                OnBundleProgressReport(this, percents);
        }

        public virtual string DeviceName
        {
            get { return DeviceIo.ShortId; }
        }

        #endregion
    }
}
