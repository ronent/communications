﻿using Infrastructure.Communication;
using SocketClient.Utilities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SocketClient.Devices.Optimizations
{
    public delegate void FinishedLoopThroughDelegate(object sender, FinishedLoopThroughAsLongEventArgs e);

    public class LoopThroughAsLongAggregate
    {
        public event FinishedLoopThroughDelegate OnFinishedLoopThrough;
        public event ProgressReportDelegate OnProgressReport;

        public int Count { get { return packetsToSendToServer.Count; } }

        private DeviceIO device;
        private LoopArgs loopArgs;
        ManualResetEvent sendingMre = new ManualResetEvent(false);
        List<byte[]> packetsToSendToServer = new List<byte[]>();
        private bool isClosed = false;

        public LoopThroughAsLongAggregate(DeviceIO device, LoopArgs args)
        {
            this.device = device;
            loopArgs = args;
        }

        public void Start()
        {
            device.OnDeviceClose += device_OnDeviceClose;

            Log.Log.WriteDebug(string.Format("Socket Client => Device: '{0}' started loop through, {1} packets expected.", device.DeviceName, loopArgs.ExpectedCount));

            if (isCountWithinLimits())
            {
                startFunnelingDataFromLogger();
                sendOpCodeToDevice();
            }
            else
                invokeOnFinished();
        }

        void device_OnDeviceClose()
        {
            isClosed = true;
        }

        private void startFunnelingDataFromLogger()
        {
            device.DeviceIo.OnDataReceived += DeviceIO_OnDataReceived;
        }

        private void stopFunnelingDataFromLogger()
        {
            device.DeviceIo.OnDataReceived -= DeviceIO_OnDataReceived;
        }

        void DeviceIO_OnDataReceived(object sender, DataTransmittedEventArgs e)
        {
            if (shouldIgnoreOutput(e.Data) || isClosed)
                return;

            Task.Factory.StartNew(() =>
                {
                    sendingMre.Set();

                    if (isValidOpcodeResult(e.Data))
                    {
                        packetsToSendToServer.Add(e.Data);
                        invokOnProgressReport();

                        if (isCountWithinLimits())
                            sendOpCodeToDevice();
                        else
                            invokeOnFinished();
                    }
                    else
                        invokeOnFinished();
                });
        }

        private object sendingLocker = new object();
        private void sendOpCodeToDevice()
        {
            if (isClosed) 
                return;

            Task.Factory.StartNew(() =>
            {
                lock (sendingLocker)
                {
                    sendingMre.Reset();

                    int retries = loopArgs.Retries;
                    while (retries-- > 0)
                    {
                        device.DeviceIo.SendData(loopArgs.OpCode);
                        if (sendingMre.WaitOne(loopArgs.RetryIntervalms))
                            return;
                    }
                }
            });
        }

        private bool isCountWithinLimits()
        {
            return loopArgs.ExpectedCount == null || Count < loopArgs.ExpectedCount;
        }

        private bool isValidOpcodeResult(byte[] returnOpcode)
        {
            return isValidOutput(loopArgs.ReturnOpCodesToProcess, returnOpcode);
        }

        private bool shouldIgnoreOutput(byte[] returnOpcode)
        {
            bool returnOpCodesToIgnore = isValidOutput(loopArgs.ReturnOpCodesToIgnore, returnOpcode);
            bool ignoredOpCodes = isValidOutput(device.IgnoredOpCodes.OpCodes, returnOpcode);

            return returnOpCodesToIgnore || ignoredOpCodes;
        }

        private bool isValidOutput(IEnumerable<byte[]> outputPool, byte[] returnOpcode)
        {
            try
            {
                return outputPool.Any(allowedOpcode =>
                {
                    int index = loopArgs.OpCodeStartIndex;
                    foreach (byte b in allowedOpcode)
                        if (returnOpcode[index++] != b)
                            return false;

                    return true;
                });
            }
            catch
            {
                return false;
            }
        }

        private void invokOnProgressReport()
        {
            if (OnProgressReport != null)
                OnProgressReport.Invoke(this, (int)(100 * packetsToSendToServer.Count / loopArgs.ExpectedCount));
        }

        private void invokeOnFinished()
        {
            stopFunnelingDataFromLogger();
            device.OnDeviceClose -= device_OnDeviceClose;

            Log.Log.WriteDebug(string.Format("Socket Client => Device: '{0}' finished loop through {1}/{2}.", device.DeviceName, packetsToSendToServer.Count, loopArgs.ExpectedCount));

            if (OnFinishedLoopThrough != null)
                OnFinishedLoopThrough.Invoke(this, new FinishedLoopThroughAsLongEventArgs(packetsToSendToServer));
        }
    }
}
