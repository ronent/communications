﻿using System;
using System.Collections.Generic;

namespace SocketClient.Devices.Optimizations
{
    public class FinishedLoopThroughAsLongEventArgs : EventArgs
    {
        public IEnumerable<byte[]> Packets { get; private set; }
        public Exception Exception { get; set; }
        public bool IsOK { get { return Exception == null; } }

        public FinishedLoopThroughAsLongEventArgs(IEnumerable<byte[]> packets)
        {
            this.Packets = packets;
        }
    }
}
