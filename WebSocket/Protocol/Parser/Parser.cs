﻿using SocketProtocol.Messages;
using SocketProtocol.Messages.Arguments;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Parser
{
    public delegate void SendMessageDelegate(byte[] data);
    public delegate void LogMessageDelegate(string message);

    [Serializable]
    public class Parser
    {
        #region Fields
        private List<BaseMessage> messageInstances;

        public Bundle BundleCommand;
        public DeviceConnection DeviceConnectionCommand;
        public DeviceIDs DeviceIDsCommand;
        public IgnoreOpCodes IgnoreOpCodesCommand;
        public Packet PacketCommand;
        public BundleProgress BundleProgressCommand;

        /// <summary>
        /// Gets all OpCodes fields that are defined in the wrapper
        /// </summary>
        private IEnumerable<FieldInfo> messagesFields
        {
            get
            {
                return from FieldInfo field in this.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance)
                       where field.FieldType.IsSubclassOf(typeof(BaseMessage))
                       select field;
            }
        }

        /// <summary>
        /// Turns OpCode fieldInfo into the actual instances of type 'OpCode'
        /// </summary>
        private IEnumerable<BaseMessage> messagesInstances
        {
            get
            {
                if (messageInstances == null)
                {
                    messageInstances = (from field in messagesFields
                                       select field.GetValue(this) as BaseMessage).ToList();
                }

                return messageInstances;
            }
        }

        #endregion
        #region Constructor
        public Parser()
        {
            initializeSupportedMessages();
        }

        private void initializeSupportedMessages()
        {
            foreach (FieldInfo field in messagesFields)
            {
                field.SetValue(this, Activator.CreateInstance(field.FieldType));
            }
        }

        #endregion
        #region Events
        /// <summary>
        /// Occurs when a new message shoule sent via the communication layer.
        /// </summary>
        public event SendMessageDelegate OnSendMessage;

        /// <summary>
        /// Occurs when a new log message was writen.
        /// </summary>
        public event LogMessageDelegate OnLogMessage;

        #endregion
        #region Parse
        public void Parse(byte[] data)
        {
            Parse(data, string.Empty);
        }

        public void Parse(byte[] data, string id)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    BaseMessage msg = (BaseMessage)deserialize(data);

                    if (id != string.Empty)
                        msg.ClientId = id;

                    foreach (var command in messagesInstances)
                    {
                        if (command.Execute(msg))
                            return;
                    }
       
                }
                catch (Exception ex)
                {
                    if (ex.Message != "The given key was not present in the dictionary.")
                        invokeOnLogMessage("Exception in parser: " + ex.Message);
                }
            });
        }

        #endregion
        #region Messages
        /// <summary>
        /// Sends a message to the device.
        /// </summary>
        /// <param name="message">The message.</param>
        public void SendMessage(BaseMessage message)
        {
            if (OnSendMessage != null)
            {
                byte[] arr = serialize(message);
                OnSendMessage.Invoke(arr);
            }
        }

        private void invokeOnLogMessage(string message)
        {
            if (OnLogMessage != null)
                OnLogMessage.Invoke(message);
        }

        #endregion
        #region Serialize / Deserialize
        private static byte[] serialize(BaseMessage message)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, message);
                return ms.ToArray();
            }
        }

        private static BaseMessage deserialize(byte[] array)
        {
            using (MemoryStream ms = new MemoryStream(array))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (BaseMessage)formatter.Deserialize(ms);
            }
        }

        #endregion
    }
}
