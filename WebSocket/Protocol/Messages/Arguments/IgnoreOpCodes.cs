﻿using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages.Arguments
{
    public delegate void IgnoreOpCodesDelegate(IgnoreOpCodes message);

    [Serializable]
    public class IgnoreOpCodes : DeviceMessage
    {
        #region Events
        public event IgnoreOpCodesDelegate OnIgnoreOpCodes;

        #endregion
        #region Command
        public const string command = "IgnoreOpCodes";
        public override string Command { get { return command; } }

        #endregion
        #region Arguments
        public IEnumerable<byte[]> OpCodesToIgnore { get; set; }
        public int OpCodeStartByte { get; set; }

        #endregion
        #region Abstract Implementation
        public override void ExecuteEvent(IBaseMessage message)
        {
            if (OnIgnoreOpCodes != null)
                OnIgnoreOpCodes(message as IgnoreOpCodes);
        }

        #endregion
    }
}
