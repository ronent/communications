﻿using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages.Arguments
{
    public delegate void BundleProgressDelegate(BundleProgress message);

    [Serializable]
    public class BundleProgress : DeviceMessage
    {
        #region Events
        public event BundleProgressDelegate OnBundleProgress;

        #endregion
        #region Command
        public const string command = "BundleProgress";
        public override string Command { get { return command; } }

        #endregion
        #region Arguments
        public int Percents { get; set; }

        #endregion 
        #region Abstract Implementation
        public override void ExecuteEvent(IBaseMessage message)
        {
            if (OnBundleProgress != null)
                OnBundleProgress(message as BundleProgress);
        }

        #endregion
    }
}
