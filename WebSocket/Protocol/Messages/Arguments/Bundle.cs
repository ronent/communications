﻿using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages.Arguments
{
    public delegate void BundleDelegate(Bundle message);

    [Serializable]
    public class Bundle : DeviceMessage
    {
        #region Events
        public event BundleDelegate OnBundle;

        #endregion
        #region Command
        public const string command = "Bundle";
        public override string Command { get { return command; } }

        #endregion
        #region Arguments
        public IEnumerable<byte[]> Packets { get; set; }
        public LoopArgs LoopArguments { get; set; }

        #endregion
        #region Abstract Implementation
        public override void ExecuteEvent(IBaseMessage message)
        {
            if (OnBundle != null)
                OnBundle(message as Bundle);
        }

        #endregion
    }
}
