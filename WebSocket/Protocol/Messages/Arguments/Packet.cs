﻿using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages.Arguments
{
    public delegate void PacketDelegate(Packet message);

    [Serializable]
    public class Packet : DeviceMessage
    {
        #region Events
        public event PacketDelegate OnPacket;

        #endregion
        #region Command
        public const string command = "Packet";
        public override string Command { get { return command; } }

        #endregion
        #region Arguments
        public byte[] Data { get; set; }

        #endregion
        #region Abstract Implementation
        public override void ExecuteEvent(IBaseMessage message)
        {
            if (OnPacket != null)
                OnPacket(message as Packet);
        }

        #endregion
    }
}
