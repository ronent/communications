﻿using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages.Arguments
{
    public delegate void DeviceConnectionDelegate(DeviceConnection message);

    [Serializable]
    public class DeviceConnection : DeviceMessage
    {
        #region Events
        public event DeviceConnectionDelegate OnDeviceConnection;

        #endregion
        #region Command
        public const string command = "DeviceAdded/Removed";
        public override string Command { get { return command; } }

        #endregion
        #region Arguments
        public enum eStatus
        {
            Connected,
            Disconnected
        }

        public eStatus Status { get; set; }
        public DeviceID DeviceID { get; set; }

        #endregion
        #region Abstract Implementation
        public override void ExecuteEvent(IBaseMessage message)
        {
            if (OnDeviceConnection != null)
                OnDeviceConnection(message as DeviceConnection);
        }

        #endregion
    }
}
