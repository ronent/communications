﻿using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using SocketProtocol.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages.Arguments
{
    public delegate void DeviceIdsDelegate(DeviceIDs message);

    [Serializable]
    public class DeviceIDs : BaseMessage
    {
        #region Events
        public event DeviceIdsDelegate OnDeviceIds;

        #endregion
        #region Command
        public const string command = "DeviceIds";
        public override string Command { get { return command; } }

        #endregion
        #region Arguments
        public List<DeviceID> DeviceIds { get; set; }

        #endregion
        #region Abstract Implementation
        public override void ExecuteEvent(IBaseMessage message)
        {
            if (OnDeviceIds != null)
                OnDeviceIds(message as DeviceIDs);
        }

        public override eMessageType Type
        {
            get { return eMessageType.Internal; }
        }

        #endregion
    }
}
