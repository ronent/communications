﻿using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages
{
    [Serializable]
    public abstract class DeviceMessage : BaseMessage
    {
        public string DevicePath { get; set; }

        public override eMessageType Type
        {
            get { return eMessageType.Internal; }
        }
    }
}
