﻿using Infrastructure.Communication.DeviceIO;
using SocketProtocol.Parser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketProtocol.Messages
{
    [Serializable]
    public abstract class BaseMessage : IBaseMessage
    {
        #region Abstract Members
        public abstract string Command { get; }
        public abstract eMessageType Type { get; }

        #endregion
        #region Members
        public string ClientId { get; set; }

        #endregion
        #region Abstract Methods
        public abstract void ExecuteEvent(IBaseMessage message);

        #endregion
        #region Methods
        public bool Execute(IBaseMessage message)
        {
            try
            {
                if (this.Command == message.Command)
                {
                    ExecuteEvent(message);
                    return true;
                }
            }
            catch { }

            return false;
        }

        #endregion
    }
}
