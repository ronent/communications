﻿using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using SocketProtocol.Parser;
using System;
using System.Collections.Generic;

namespace SocketCommunication.Devices
{
    public delegate void InvokeLoopThroughAsLongdelegate(string id, LoopArgs args);

    public class DeviceIO : IDeviceIO, IBundle
    {
        #region Members
        public DeviceID DeviceID { get; private set; }
        protected string devicePath { get; private set; }

        #endregion
        #region Constructor
        public DeviceIO(string devicePath, DeviceID deviceID)
        {
            this.devicePath = devicePath;
            this.DeviceID = deviceID;
        }

        #endregion
        #region IDeviceIO
        public event DataReceivedEventHandler OnDataReceived;
        public event DataSentEventHandler OnDataSent;

        public string Id
        {
            get { return devicePath; }
        }

        public string ShortId
        {
            get { return Id.Split('&')[2]; }
        }

        public void SendData(byte[] data)
        {
            if (OnDataSent != null)
                OnDataSent.Invoke(this, new DataTransmittedEventArgs(data));
        }

        public void ReceiveData(byte[] data)
        {
            if (OnDataReceived != null)
                OnDataReceived.Invoke(this, new DataTransmittedEventArgs(data));
        }

        public void Dispose()
        {

        }

        #endregion
        #region Bundle
        public event InvokeLoopThroughAsLongdelegate OnInvokeLoopThroughAsLong;
        public event DataReceivedEventHandler OnBundleReceived;
        public event ReportProgressDelegate OnProgressReported;

        public bool IsBundleSupported
        {
            get { return true; }
        }

        public void ReceiveBundle(IEnumerable<byte[]> packets)
        {
            if (OnBundleReceived != null)
                OnBundleReceived.Invoke(this, new DataTransmittedEventArgs(packets));
        }

        public void InvokeLoopThroughAsLong(LoopArgs args)
        {
            if (OnInvokeLoopThroughAsLong != null)
                OnInvokeLoopThroughAsLong(Id, args);
        }

        public void InvokeOnProgressReported(int percents)
        {
            if (OnProgressReported != null)
                OnProgressReported(percents);
        }

        #endregion
    }
}
