﻿using Fleck;
using Infrastructure.Communication;
using SocketCommunication.Devices;
using SocketProtocol.Messages;
using SocketProtocol.Messages.Arguments;
using SocketProtocol.Parser;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace SocketCommunication.Communication
{
    public class SocketManager
    {
        protected IWebSocketConnection socket;
        protected ConcurrentDictionary<string, DeviceIO> devices;
        protected Parser clientSocketParser;
        protected CommunicationManager parent;

        public SocketManager(IWebSocketConnection socketConnection, CommunicationManager communicationManager)
        {
            socket = socketConnection;
            parent = communicationManager;

            InitializeParser();
            initializeParserEvents();

            socketConnection.OnBinary = (data) =>
            {
                clientSocketParser.Parse(data, socket.ConnectionInfo.Id.ToString());
            };
        }

        protected virtual void InitializeParser()
        {
            clientSocketParser = new Parser();
        }

        private void initializeParserEvents()
        {
            InitializeParserEvents();

            devices = new ConcurrentDictionary<string, DeviceIO>();

            clientSocketParser.OnLogMessage += (message) =>
            {
                parent.InvokeLog(message);
            };

            clientSocketParser.OnSendMessage += (data) =>
            {
                sendMessageToAgent(data);
            };

            clientSocketParser.DeviceIDsCommand.OnDeviceIds += (message) =>
            {
                sendDeviceIdsToAgent(message);
            };

            clientSocketParser.DeviceConnectionCommand.OnDeviceConnection += (message) =>
            {
                deviceConnection(message.DeviceID, message.DevicePath, message.Status);
            };

            clientSocketParser.BundleCommand.OnBundle += (message) =>
            {
                sendBundleToDevice(message.DevicePath, message.Packets);
            };

            clientSocketParser.PacketCommand.OnPacket += (message) =>
            {
                sendDataToDevice(message.DevicePath, message.Data);
            };

            clientSocketParser.BundleProgressCommand.OnBundleProgress += (message) =>
                {
                    sendBundleProgressToDevice(message.DevicePath, message.Percents);
                };
        }

        protected virtual void InitializeParserEvents() { }

        private void sendDeviceIdsToAgent(BaseMessage message)
        {
            DeviceIDs msg = new DeviceIDs()
            {
                ClientId = message.ClientId,
                DeviceIds = parent.Ids,
            };

            parent.InvokeLog(string.Format("sending device ids to client: {0}", message.ClientId));
            clientSocketParser.SendMessage(msg);
        }

        private void sendMessageToAgent(byte[] data)
        {
            socket.Send(data);
        }

        private void sendDataToDevice(string devicePath, byte[] data)
        {
            devices[devicePath].ReceiveData(data);
        }

        private void sendBundleToDevice(string devicePath, IEnumerable<byte[]> bundleData)
        {
            devices[devicePath].ReceiveBundle(bundleData);
        }

        private void sendBundleProgressToDevice(string devicePath, int percents)
        {
            devices[devicePath].InvokeOnProgressReported(percents);
        }

        private void deviceConnection(DeviceID deviceId, string devicePath, DeviceConnection.eStatus connection)
        {
            switch (connection)
            {
                case DeviceConnection.eStatus.Connected:
                    addDevice(deviceId, devicePath);
                    break;
                case DeviceConnection.eStatus.Disconnected:
                    removeDevice(deviceId, devicePath);
                    break;
                default:
                    break;
            }
        }

        private void removeDevice(DeviceID deviceId, string devicePath)
        {
            try
            {
                DeviceIO removedDevice;

                if (devices.TryGetValue(devicePath, out removedDevice))
                {
                    parent.InvokeOnDeviceDisonnected(this, new HIDConnectionEventArgs() { DeviceID = deviceId, DeviceIO = removedDevice, UID = removedDevice.Id });
                }

                devices.TryRemove(devicePath, out removedDevice);
                try { removedDevice.Dispose(); }
                catch { };
            }
            catch (ObjectDisposedException) { }
            catch (Exception ex)
            {
                parent.InvokeLog("On RemoveDevice", ex);
            }
        }

        private void addDevice(DeviceID deviceId, string devicePath)
        {
            DeviceIO deviceIO = GenerateNewDeviceIO(devicePath, deviceId);
            registerDeviceEvents(deviceIO);

            devices.TryAdd(devicePath, deviceIO);

            parent.InvokeOnDeviceConnected(this, new HIDConnectionEventArgs() { DeviceID = deviceId, DeviceIO = deviceIO, UID = deviceIO.Id });
        }

        protected virtual DeviceIO GenerateNewDeviceIO(string devicePath, DeviceID deviceId)
        {
            return new DeviceIO(devicePath, deviceId);
        }

        private void registerDeviceEvents(DeviceIO deviceIO)
        {
            deviceIO.OnDataSent += (sender, e) =>
                {
                    clientSocketParser.SendMessage(new Packet()
                    {
                        Data = e.Data,
                        DevicePath = (sender as DeviceIO).Id,
                    });
                };

            deviceIO.OnInvokeLoopThroughAsLong += (id, args) =>
                {
                    clientSocketParser.SendMessage(new Bundle()
                    {
                        DevicePath = id,
                        LoopArguments = args,
                    });
                };
        }

        public void Dispose()
        {
            foreach (var device in devices)
            {
                removeDevice(device.Value.DeviceID, device.Key);
                device.Value.Dispose();
            }

            socket.Close();
        }
    }
}


