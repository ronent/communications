﻿using Fleck;
using Infrastructure.Communication;
using Infrastructure.Communication.Modules;
using Infrastructure.Communication.Modules.HID;
using Infrastructure.Communication.Modules.WebAgent;
using Log;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace SocketCommunication.Communication
{
    [Export(typeof(ICommunicationManager))]
    [ExportMetadata("Name", "Socket Communication")]
    public class CommunicationManager : IHIDCommunicationManager, ILog
    {
        #region Fields
        private bool isSSL = false;

        #endregion
        #region Members
        public event MessageLoggedDelegate OnMessageLogged;

        private WebSocketServer webSocketServer;
        protected ConcurrentDictionary<string, SocketManager> clientScokets;
        private List<DeviceID> deviceIds;

        #endregion
        #region Constructor
        public CommunicationManager()
        {
            deviceIds = new List<DeviceID>();
        }

        #endregion
        #region Properties
        public virtual string ModuleName { get { return "Socket Communication"; } }

        #endregion
        #region ICommunicationManager
        public event DeviceConnectedDelegate OnDeviceConnected;
        public event DeviceRemovedDelegate OnDeviceRemoved;

        public List<DeviceID> Ids
        {
            get { return deviceIds; }
        }

        public void RegisterDeviceIds(List<DeviceID> listDeviceIds)
        {
            InvokeLog(string.Format("registered device ids: {0}", string.Join<DeviceID>(", ", listDeviceIds)));
            deviceIds.AddRange(listDeviceIds);
            deviceIds = deviceIds.Distinct().ToList();
        }

        public void UnregisterDeviceIds()
        {
            deviceIds.Clear();
        }

        public void ScanForDevices()
        {
            InitializeClientScokets();
            InvokeLog(string.Format("started scanning for devices on port: '{0}'", webSocketServer.Port));
            startSocketServer();
        }

        public void Shutdown()
        {
            try
            {
                foreach (var item in clientScokets)
                {
                    item.Value.Dispose();
                }

                clientScokets.Clear();
                webSocketServer.Dispose();
            }
            catch { }
        }

        #endregion
        #region Fleck
        protected void InitializeClientScokets()
        {
            clientScokets = new ConcurrentDictionary<string, SocketManager>();

            if (File.Exists("cer.cer") && isSSL)
            {
                webSocketServer = new WebSocketServer("wss://" + ConfigurationManager.AppSettings["WebSocketListenerEndPoint"]);

                webSocketServer.Certificate = new X509Certificate2(X509Certificate.CreateFromCertFile("cer.cer"));
                InvokeLog("X509Certificate2 was loaded.");
            }
            else
                webSocketServer = new WebSocketServer("ws://" + ConfigurationManager.AppSettings["WebSocketListenerEndPoint"]);
        }
        
        #endregion
        #region Methods
        private void startSocketServer()
        {
            try
            {
                webSocketServer.Start(socket =>
                {
                    socket.OnOpen = () =>
                    {
                        InvokeLog(string.Format("new socket connected: {0}", socket.ConnectionInfo.Id));

                        clientScokets.TryAdd(socket.ConnectionInfo.Id.ToString(), GenerateNewSocketManager(socket));
                    };

                    socket.OnClose = () =>
                    {
                        removeSocket(socket.ConnectionInfo.Id.ToString());
                    };

                    socket.OnError = (exception) =>
                    {
                        if (!socket.IsAvailable)
                        {
                            removeSocket(socket.ConnectionInfo.Id.ToString());
                        }

                        if (!exception.Message.Contains("Unable to read data from the transport connection"))
                        {
                            InvokeLog(string.Format("Socket error: {0}", socket.ConnectionInfo.Id), exception);
                        }
                    };
                });
            }
            catch (Exception ex) 
            {
                InvokeLog("On StartSocketServer", ex);
            }
        }

        protected virtual SocketManager GenerateNewSocketManager(IWebSocketConnection socket)
        {
            return new SocketManager(socket, this);
        }

        internal void InvokeLog(string message)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, string.Format("[{0}] ==> {1}", ModuleName, message));
        }

        internal void InvokeLog(string message, Exception exception)
        {
            if (OnMessageLogged != null)
                OnMessageLogged(this, message + Environment.NewLine + exception.Message);
        }

        internal void InvokeOnDeviceConnected(object sender, HIDConnectionEventArgs e)
        {
            InvokeLog(string.Format("new device connected {0}", e.DeviceIO.ShortId));

            if (OnDeviceConnected != null)
                OnDeviceConnected.Invoke(sender, new WebAgentConnectionEventArgs() { DeviceID = e.DeviceID, DeviceIO = e.DeviceIO, UID = e.UID, CustomerId = 1 });
        }

        internal void InvokeOnDeviceDisonnected(object sender, ConnectionEventArgs e)
        {
            InvokeLog(string.Format("device disconnected {0}", e.DeviceIO.ShortId));

            if (OnDeviceRemoved != null)
                OnDeviceRemoved.Invoke(sender, e);
        }

        private void removeSocket(string id)
        {
            InvokeLog(string.Format("Socket is closed: {0}", id));

            SocketManager removedSocket;
            if (clientScokets.TryRemove(id, out removedSocket))
                removedSocket.Dispose();
        }

        #endregion
    }
}
