﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFClient.Service;
using WCFClient.ServiceReference;
using WeboomerangClient.Communications;
using WeboomerangClient.ServiceReference;

namespace WeboomerangClient.Service
{
    public class ServiceManager : WCFClient.Service.ServiceManager, IWeboomerangServer, IServer, IServiceManager
    {
        #region Fields
        protected WeboomerangServerClient server;

        #endregion
        #region Constructor
        public ServiceManager(string uri, ClientAPI client)
            :base(uri, client)
        {

        }

        #endregion
        #region Override Methods
        protected override void initializeServer()
        {
            server = new ServiceReference.WeboomerangServerClient(new InstanceContext(client), instantiateBinding(), instantiateAddress());
        }

        #endregion
        #region IServiceManager
        public override void Close()
        {
            if (server.State != CommunicationState.Closed)
            {
                try
                {
                    server.Abort();
                    server.Close();
                }
                catch { }
            }
        }

        public override event EventHandler Faulted
        {
            add { server.InnerChannel.Faulted += value; }
            remove { server.InnerChannel.Faulted -= value; }
        }

        private void tryOpenChannel()
        {
            while (State != CommunicationState.Opened)
            {
                try
                {
                    if(server==null)
                        initializeServer();
                     server.Open();
                }
                catch
                {
                    if (server != null)
                        server.Abort();
                    server = null;

                }
            }
        }

        public override void Open()
        {
            if (server == null)
                initializeServer();

           tryOpenChannel();
        }

        public override CommunicationState State
        {
            get
            {
                return server != null ? server.State : CommunicationState.Closed;
            }
        }

        #endregion
        #region IServer
        public override bool KeepAlive()
        {
            try
            {
                return server.KeepAlive();
            }
            catch 
            {
                Close();
            }

            return false;
        }

        public override void OnBundleReceived(string id, byte[][] data)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnBundleReceived(id, data);
            }
            catch 
            {
                Close();
            }
        }

        public override void OnDataReceived(string id, byte[] data)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnDataReceived(id, data);
            }
            catch 
            {
                Close();
            }
        }

        public override void OnDeviceConnected(Infrastructure.Communication.ConnectionEventArgs args)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnDeviceConnected(args);
            }
            catch 
            {
                Close();
            }
        }

        public override void OnDeviceRemoved(Infrastructure.Communication.ConnectionEventArgs args)
        {
            try
            {
                if (!IsOpen)
                    return;

                server.OnDeviceRemoved(args);
            }
            catch 
            {
                Close();
            }
        }

        public override bool OpenSession()
        {
            try
            {
                return server.OpenSession();
            }
            catch 
            {
                Close();
            }

            return false;
        }

        #endregion
    }
}