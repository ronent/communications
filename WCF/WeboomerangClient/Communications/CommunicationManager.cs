﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeboomerangClient.Communications;

namespace WeboomerangClient
{
    public class CommunicationManager : WCFClient.CommunicationManager
    {
        #region Constructor
        public CommunicationManager(string serverURI)
            :base(serverURI)
        {

        }

        #endregion
        #region Override Methods
        protected override void initializeDeviceManager()
        {
            deviceManager = new DeviceManager(this);
        }

        protected override void initializeServerAPI()
        {
            server = new ServerAPI(this);
        }

        protected override void initializeClientAPI()
        {
            client = new ClientAPI(this);
        }

        #endregion
        #region Properties
        protected internal ServerAPI server { get { return base.server as ServerAPI; } set { base.server = value; } }
        protected internal ClientAPI client { get { return base.client as ClientAPI; } set { base.client = value; } }
        protected internal DeviceManager deviceManager { get { return base.deviceManager as DeviceManager; } set { base.deviceManager = value; } }

        #endregion
    }
}
