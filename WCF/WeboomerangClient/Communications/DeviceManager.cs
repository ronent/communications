﻿using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WCFClient.Devices;

namespace WeboomerangClient.Communications
{
    public class DeviceManager : WCFClient.Communications.DeviceManager
    {
        #region Constructor
        public DeviceManager(CommunicationManager communicationManager)
            :base(communicationManager)
        {

        }

        #endregion
        #region Override Methods
        protected override DeviceIO GenerateDeviceIO(ConnectionEventArgs e)
        {
            return new Devices.DeviceIO(parent.server, e.DeviceIO);
        }

        new public Devices.DeviceIO GetDevice(string id)
        {
            try
            {
                return clientDevices[id] as Devices.DeviceIO;
            }
            catch { }

            return null;
        }

        #endregion
        #region Methods
        protected CommunicationManager parent { get { return base.parent as CommunicationManager; } set { base.parent = value; } }
        
        #endregion
    }
}
