﻿using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCFCommunication;

namespace WeboomerangCommunication
{
    public class DeviceIO : WCFCommunication.DeviceIO, IDeviceIOExtra
    {
        #region Members

        #endregion
        #region Constructor
        public DeviceIO(Server server, string id)
            :base(server, id)
        {

        }

        #endregion
        #region Properties
        private Server parent { get { return base.parent as Server; } }

        #endregion
        #region Methods
        public void OnDeviceIdentification(string serialNumber, string comment)
        {
            parent.InvokeOnDeviceIdentification(Id, serialNumber, comment);
        }

        public void OnStatusString(string status, bool isError)
        {
            parent.InvokeOnStatusString(Id, status, isError);
        }

        public void OnProgressReport(int percents)
        {
            parent.InvokeOnProgressReport(Id, percents);
        }

        public void OnBoomerangReport(string uri, string status, string finishStatus)
        {
            parent.InvokeOnBoomerangReport(Id, uri, status, finishStatus);
        }

        #endregion
    }
}
