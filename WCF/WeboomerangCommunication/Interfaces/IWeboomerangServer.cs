﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFCommunication.Interfaces;

namespace WeboomerangCommunication.Interfaces
{
    [ServiceContract(CallbackContract = typeof(IWeboomerangClient))]
    interface IWeboomerangServer : IServer
    {

    }
}
