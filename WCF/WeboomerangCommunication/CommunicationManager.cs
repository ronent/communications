﻿using Infrastructure.Communication.Modules;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeboomerangCommunication.Interfaces;

namespace WeboomerangCommunication
{
    [Export(typeof(ICommunicationManager))]
    [ExportMetadata("Name", "Weboomerang Communication")]
    public class CommunicationManager : WCFCommunication.CommunicationManager
    {
        #region Constructor
        public CommunicationManager()
            :base()
        {

        }

        #endregion
        #region Properties
        protected override string wcfEndPoint { get { return "WCFCommunication/Weboomerang"; } }


        #endregion
        #region Override Methods
        protected override void initializeServiceHost()
        {
            host = new System.ServiceModel.ServiceHost(typeof(Server), Endpoint);
        }

        protected override Type getServerType()
        {
            return typeof(IWeboomerangServer);
        }

        #endregion
    }
}
