﻿using Infrastructure.Communication;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WCFClient.Utilities;
using Auxiliary.Extentions;
using HidLibrary;

namespace WCFClient.Devices.Optimizations
{
    public delegate void LoopThroughDelegate(object sender, FinishedLoopThroughAsLongEventArgs e);
    public delegate void FinishedLoopThroughDelegate(object sender, FinishedLoopThroughAsLongEventArgs e);

    public class LoopThroughAsLongAggregate : IDisposable
    {
        #region Fields
        public event LoopThroughDelegate OnBundle;
        public event FinishedLoopThroughDelegate OnFinished;
        public event ProgressReportDelegate OnProgressReport;
        public event Action OnDownloading;

        public int Count { get; private set; }
        public static int MaxBundleSize = 32;

        private DeviceIO device;
        private LoopArgs loopArgs;
        private ManualResetEventSlim sendingMre = new ManualResetEventSlim(false);
        private List<byte[]> packetsToSendToServer = new List<byte[]>();
        private BlockingCollection<byte[]> dataReceivedQueue;
        private volatile bool isDisposed;
        private Stopwatch stopper = new Stopwatch();
        private DateTime lastSent;

        private volatile bool isRunning;
        public bool IsRunning { get { return isRunning; } private set { isRunning = value; } }

        #endregion
        #region Constructor
        public LoopThroughAsLongAggregate(DeviceIO device, LoopArgs args)
        {
            dataReceivedQueue = new BlockingCollection<byte[]>();
            this.device = device;
            IsDisposed = false;
            IsRunning = false;
            loopArgs = args;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get { return isDisposed; } private set { isDisposed = value; } }

        #endregion
        #region Public Methods
        public void Start()
        {
            if (loopArgs.ExpectedCount == uint.MaxValue)
            {
                Log.Instance.Write("[WCF Client]  Device '{0}' Error: expected packet count is Int.Max", this, Log.LogSeverity.DEBUG, device.Name);
                return;
            }

            lastSent = DateTime.Now;

            try
            {
                Log.Instance.Write("[WCF Client]  Device '{0}' started loop through, {1} packets expected.", this, Log.LogSeverity.DEBUG, device.Name, loopArgs.ExpectedCount);

                if (isCountWithinLimits())
                {
                    stopper.Start();
                    startFunnelingDataFromLogger();

                    dataReceivedQueueListener();
                    sendOpCodeToDevice();
                }
                else
                    invokeOnFinished();
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("[WCF Client]  Fail in start bundle download", this, ex);
            }
        }

        #endregion
        #region Private Methods
        private void startFunnelingDataFromLogger()
        {
            IsRunning = true;
            device.OnDataReceived += DeviceIO_OnDataReceived;
        }

        private void stopFunnelingDataFromLogger()
        {
            device.OnDataReceived -= DeviceIO_OnDataReceived;
            IsRunning = false;
        }

        void DeviceIO_OnDataReceived(object sender, DataTransmittedEventArgs e)
        {
            if (sendingMre != null && !sendingMre.IsSet)
                sendingMre.Set();

            if (device.IsConnected)
                if (dataReceivedQueue != null)
                    dataReceivedQueue.Add(e.Data);
        }

        private void dataReceivedQueueListener()
        {
            Task.Factory.StartNew(() =>
            {
                while (IsRunning)
                {
                    byte[] data = dataReceivedQueue.Take();
                    dataReceived(data);
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void dataReceived(byte[] data)
        {
            //Log.Instance.Write(string.Format("[WCF Client]  Device '{0}' From: {1}", device.Name, data[1].ToString("X2")), this);

            if (shouldIgnoreOutput(data) || isDisposed)
                return;

            if (isValidOpcodeResult(data))
            {
                addPacketToBundle(data);
                invokOnProgressReport();

                if (isCountWithinLimits() && !isDisposed)
                    sendOpCodeToDevice();
                else
                    invokeOnFinished();
            }
            else
            {
                if (shouldSendImmediate(data))
                {
                    Log.Instance.Write("[WCF Client]  Device '{0}' is sending immediate OpCode: '{1}'", this, Log.LogSeverity.DEBUG, device.Name, data[1].ToString("X2"));
                    device.SendData(data); // sends the data to the server 
                }
                else if (deviceDisconnected(data))
                    invokeOnFinished();
            }
        }

        private void addPacketToBundle(byte[] data)
        {
            packetsToSendToServer.Add(data);
            Count++;
           
            if (packetsToSendToServer.Count >= MaxBundleSize)
            {
                Log.Instance.Write("[WCF Client]  Device '{0}' is sending bundle of {1} packets: {2}/{3} in {4}", this, Log.LogSeverity.DEBUG, device.Name, packetsToSendToServer.Count, Count, loopArgs.ExpectedCount, (DateTime.Now - lastSent).ToFormatedString());

                Task.Factory.StartNew(() =>
                    {
                        invokeBundle(packetsToSendToServer.ConvertAll(x => (byte[])x.Clone()));
                        packetsToSendToServer.Clear();
                    }, TaskCreationOptions.AttachedToParent).Wait();
            }
        }

        private bool deviceDisconnected(byte[] data)
        {
            foreach (var b in data)
            {
                if (!b.Equals(0x00))
                    return true;
            }

            return false;
        }

        private void sendOpCodeToDevice()
        {
            if (IsDisposed || !IsRunning)
                return;

            int retries = loopArgs.Retries;
            while (retries-- > 0 && !isDisposed)
            {
                device.SendData(loopArgs.OpCode);

                if (OnDownloading != null)
                    OnDownloading();

                if (sendingMre.Wait(loopArgs.RetryIntervalms))
                    return;
            }
        }

        private bool isCountWithinLimits()
        {
            var withinLimits = loopArgs.ExpectedCount == null || Count < loopArgs.ExpectedCount;

            if (!withinLimits && loopArgs.ReturnOpCodesToFinish != null)
            {
                if (!isLastPacketValid())
                    withinLimits = true;
            }

            return withinLimits;
        }

        private bool isLastPacketValid()
        {
            if (packetsToSendToServer.Count == 0)
                return true;

            return isValidOutput(loopArgs.ReturnOpCodesToFinish, packetsToSendToServer.Last());
        }

        private bool isValidOpcodeResult(byte[] returnOpcode)
        {
            return isValidOutput(loopArgs.ReturnOpCodesToProcess, returnOpcode);
        }

        private bool shouldIgnoreOutput(byte[] returnOpcode)
        {
            bool returnOpCodesToIgnore = isValidOutput(loopArgs.ReturnOpCodesToIgnore, returnOpcode);
            bool ignoredOpCodes = isValidOutput(device.IgnoredOpCodes.OpCodes, returnOpcode);

            return returnOpCodesToIgnore || ignoredOpCodes;
        }

        private bool shouldSendImmediate(byte[] returnOpcode)
        {
            return isValidOutput(loopArgs.ReturnOpCodesToImmediateSend, returnOpcode);
        }

        private bool isValidOutput(IEnumerable<byte[]> outputPool, byte[] returnOpcode)
        {
            try
            {
                return outputPool.Any(allowedOpcode =>
                {
                    int index = loopArgs.OpCodeStartIndex;

                    foreach (byte b in allowedOpcode)
                        if (returnOpcode[index++] != b)
                            return false;

                    return true;
                });
            }
            catch
            {
                return false;
            }
        }

        private void invokOnProgressReport()
        {
            if (!IsDisposed && OnProgressReport != null)
                OnProgressReport.BeginInvoke(this, (int)(100 * Count / loopArgs.ExpectedCount), null, null);
        }

        private void invokeOnFinished()
        {
            stopFunnelingDataFromLogger();
            stopper.Stop();

            Log.Instance.Write("[WCF Client]  Device '{0}' finished loop through {1}/{2} in {3}.", this, Log.LogSeverity.DEBUG, device.Name, Count, loopArgs.ExpectedCount, stopper.Elapsed.ToFormatedString());

            if (!IsDisposed && OnFinished != null)
            {
                OnFinished(this, new FinishedLoopThroughAsLongEventArgs(packetsToSendToServer, true));
                packetsToSendToServer.Clear();
            }
        }

        private void invokeBundle(List<byte[]> bundle)
        {
            if (!IsDisposed && OnBundle != null)
            {
                lastSent = DateTime.Now;
                Auxiliary.Tools.Utilities.InvokeEvent(OnBundle, this, new FinishedLoopThroughAsLongEventArgs(bundle, false));
            }
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                IsRunning = false;

                if (sendingMre != null && sendingMre.IsSet)
                    sendingMre.Set();

                if (stopper != null && stopper.IsRunning)
                    stopper.Stop();

                sendingMre.Dispose();
                dataReceivedQueue.Dispose();

                OnBundle = null;
                OnFinished = null;
                OnProgressReport = null;
                OnDownloading = null;
                loopArgs = null;
                packetsToSendToServer = null;
                dataReceivedQueue = null;
                stopper = null;
            }
        }

        #endregion
    }
}