﻿using Infrastructure.Communication;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WCFClient.Devices;
using WCFClient.Utilities;

namespace WCFClient.Communications
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    public class ClientAPI : ServiceReference.IServerCallback
    {
        #region Members
        protected CommunicationManager parent { get; set; }

        #endregion
        #region Constructor
        public ClientAPI(CommunicationManager communicationManager)
        {
            parent = communicationManager;
        }

        #endregion
        #region Properties
        private DeviceManager deviceManager { get { return parent.deviceManager; } }

        #endregion
        #region IClient Methods
        public void RegisterDeviceIds(DeviceID[] deviceIDs)
        {
            deviceManager.RegisterDeviceIds(deviceIDs);
            Log.Instance.Write("[WCF Client]  Registering device ids: {0}", this, Log.LogSeverity.DEBUG, string.Join<DeviceID>(", ", deviceIDs));
        }

        public void UnregisterDeviceIds()
        {
            deviceManager.UnregisterDeviceIds();
        }

        public void ScanForDevices()
        {
            deviceManager.ScanForDevices();
        }

        public void Shutdown()
        {
            deviceManager.Shutdown();
        }

        public void OnDataSend(string id, byte[] data)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.SendData(data);
        }

        public void OnDownloadProgress(string id, int percents)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.InvokeDownloadProgress(percents);
        }

        public void InvokeLoopThroughAsLong(string id, Infrastructure.Communication.LoopArgs args)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.InvokeLoopThroughAsLong(args);
        }

        public void OnIgnoreOpCodes(string id, int opCodeStartByte, byte[][] opCodesToIgnore)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.SetIgnoreOpCodes(opCodeStartByte, opCodesToIgnore);
        }

        #endregion
    }
}
