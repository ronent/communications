﻿using Infrastructure.Communication;
using Infrastructure.Communication.Modules;
using Infrastructure.Communication.Modules.HID;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WCFClient.Communications.EventArgs;
using WCFClient.Devices;

namespace WCFClient.Communications
{
    public enum eDeviceConnecionState
    {
        Connected,
        Disconnected
    }

    public delegate void DeviceConnectionChangedDelegate(DeviceConnectionEventArgs args);

    public class DeviceManager : IDisposable
    {
        #region Members
        protected CommunicationManager parent { get; set; }

        private IHIDCommunicationManager communication;
        protected ConcurrentDictionary<string, DeviceIO> clientDevices;

        #endregion
        #region Constructor
        public DeviceManager(CommunicationManager communicationManager)
        {
            IsDisposed = false;
            parent = communicationManager;

            clientDevices = new ConcurrentDictionary<string, DeviceIO>();

            communication = new HidCommunication.CommunicationManager();
            communication.OnDeviceConnected += Communication_OnDeviceConnected;
            communication.OnDeviceRemoved += Communication_OnDeviceRemoved;
        }

        #endregion
        #region Events
        public event DeviceConnectionChangedDelegate OnDeviceConnectionChanged;

        public event DeviceConnectedDelegate OnDeviceConnected;
        public event DeviceRemovedDelegate OnDeviceRemoved;

        private void Communication_OnDeviceConnected(object sender, ConnectionEventArgs e)
        {
            deviceConnected(e);
        }

        private void Communication_OnDeviceRemoved(object sender, ConnectionEventArgs e)
        {
            deviceDisconnected(e);
        }

        private void deviceConnectedHandler(ConnectionEventArgs args, DeviceIO deviceIO)
        {
            if (parent.server.IsConnected && parent.IsEnabled)
                invokeOnDeviceConnectionChanged(eDeviceConnecionState.Connected, deviceIO);

            Auxiliary.Tools.Utilities.InvokeEvent(OnDeviceConnected, this, args);
        }

        private void deviceConnected(ConnectionEventArgs e)
        {
            try
            {
                DeviceIO deviceIO = GenerateDeviceIO(e);

                clientDevices.AddOrUpdate(e.UID, deviceIO, (key, oldDevice) => deviceIO);

                if (parent.server.IsConnected && parent.IsEnabled)
                    invokeOnDeviceConnectionChanged(eDeviceConnecionState.Connected, deviceIO);

                Auxiliary.Tools.Utilities.InvokeEvent(OnDeviceConnected, this, e);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[WCF Client]  On Communication_OnDeviceConnected()", this, ex);
            }
        }

        private void deviceDisconnected(ConnectionEventArgs e)
        {
            try
            {
                DeviceIO removedDevice;
                if (clientDevices.TryRemove(e.UID, out removedDevice))
                    removedDevice.Dispose();

                invokeOnDeviceConnectionChanged(eDeviceConnecionState.Disconnected, removedDevice);

                Auxiliary.Tools.Utilities.InvokeEvent(OnDeviceRemoved, this, e);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[WCF Client]  On Communication_OnDeviceRemoved()", this, ex);
            }
        }

        protected virtual DeviceIO GenerateDeviceIO(ConnectionEventArgs e)
        {
            return new DeviceIO(parent.server, e.DeviceIO);
        }

        private void invokeOnDeviceConnectionChanged(eDeviceConnecionState state, DeviceIO deviceIO)
        {
            //Auxiliary.Tools.Utilities.InvokeEvent(OnDeviceConnectionChanged, this, new DeviceConnectionEventArgs(state, deviceIO));

            if (OnDeviceConnectionChanged != null)
                OnDeviceConnectionChanged(new DeviceConnectionEventArgs(state, deviceIO));
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        public bool IsCommunicationRegistered
        {
            get { return communication.Ids.Count > 0; }
        }

        public DateTime CommunicationRegistrationTime { get; private set; }

        public bool HasConnectedDevices
        {
            get { return clientDevices.Count > 0; }
        }

        public int ConnectedDevices
        {
            get { return clientDevices.Count; }
        }

        public DeviceIO GetDevice(string id)
        {
            try
            {
                return clientDevices[id];
            }
            catch { }

            return null;
        }

        #endregion
        #region Methods
        public void ScanForDevices()
        {
            Log.Instance.Write("[WCF Client]  Started scanning for devices", this);

            communication.ScanForDevices();
        }

        public void Shutdown()
        {
            Log.Instance.Write("[WCF Client]  Shutting down DeviceManager", this);

            removeAllDevices();
            communication.Shutdown();
        }

        private void removeAllDevices()
        {
            Parallel.ForEach(clientDevices, (pair) => 
                {
                    pair.Value.Dispose();
                    invokeOnDeviceConnectionChanged(eDeviceConnecionState.Disconnected, pair.Value);
                });

            clientDevices.Clear();
        }

        public void RegisterDeviceIds(DeviceID[] deviceIDs)
        {
            Log.Instance.Write("[WCF Client]  Registering Device Ids", this);

            communication.RegisterDeviceIds(deviceIDs.ToList());
            CommunicationRegistrationTime = DateTime.Now;
        }

        public void UnregisterDeviceIds()
        {
            Log.Instance.Write("[WCF Client]  Unregistering down Device Ids", this);

            communication.UnregisterDeviceIds();
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                communication.Dispose();
                clientDevices = null;
                parent = null;

                OnDeviceConnected = null;
                OnDeviceRemoved = null;
                OnDeviceConnectionChanged = null;
            }
        }

        #endregion
    }
}