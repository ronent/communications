﻿using Auxiliary.Tools;
using Infrastructure;
using Infrastructure.Communication;
using Infrastructure.Communication.Modules;
using Infrastructure.Communication.Modules.HID;
using Infrastructure.Communication.Modules.WebAgent;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using WCFCommunication.Interfaces;

namespace WCFCommunication
{
    [Export(typeof(ICommunicationManager))]
    [ExportMetadata("Name", "WCF Communication")]
    public class CommunicationManager : IHIDCommunicationManager, INeedLocalHost, IDisposable
    {
        #region Members
        protected ServiceHost host;
        private List<DeviceID> deviceIds;

        #endregion
        #region Constructor
        public CommunicationManager()
        {
            IsDisposed = false;
            initialize();
        }

        public CommunicationManager(string uri)
        {
            initialize();
            LocalHost = uri;
        }

        public void initialize()
        {
            deviceIds = new List<DeviceID>();
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }
        public string LocalHost { get; set; }

        protected virtual string wcfEndPoint { get { return "WCFCommunication"; } }
        private int port = 8080;

        public Uri Endpoint { get
            {
                return new Uri(String.Format("net.tcp://{0}:{1}/{2}", LocalHost, port, wcfEndPoint));
            }
        }

        #endregion
        #region IHIDCommunicationManager
        public event DeviceConnectedDelegate OnDeviceConnected;
        public event DeviceRemovedDelegate OnDeviceRemoved;

        public List<DeviceID> Ids
        {
            get { return deviceIds; }           
        }

        public void RegisterDeviceIds(List<DeviceID> deviceIDs)
        {
            deviceIds.AddRange(deviceIDs);
            deviceIds = deviceIds.Distinct().ToList();
        }

        public void UnregisterDeviceIds()
        {
            deviceIds.Clear();            
        }

        public void ScanForDevices()
        {
            initializeWCF();

            Log.Instance.Write("[WCF Communication] ==> Started scanning for devices on '{0}'", this, Log.LogSeverity.DEBUG, host.BaseAddresses[0]);
            startWCFListener();
        }

        public void Shutdown()
        {
            Dispose();
        }

        #endregion
        #region HIDCommunicationManager Methods
        internal void InvokeOnDeviceConnected(object sender, HIDConnectionEventArgs e)
        {
            Log.Instance.Write("[WCF Communication] ==> Device connected '{0}'", this, Log.LogSeverity.DEBUG, e.DeviceIO.ShortId);

            Utilities.InvokeEvent(OnDeviceConnected, sender, new WebAgentConnectionEventArgs() { DeviceID = e.DeviceID, DeviceIO = e.DeviceIO, UID = e.UID, CustomerId = 1 });
        }

        internal void InvokeOnDeviceDisonnected(object sender, ConnectionEventArgs e)
        {
            Log.Instance.Write("[WCF Communication] ==> Device disconnected: '{0}'", this, Log.LogSeverity.DEBUG, e.DeviceIO.ShortId);

            Utilities.InvokeEvent(OnDeviceRemoved, sender, e);
        }

        #endregion
        #region WCF Methods
        private void initializeWCF()
        {
            initializeServiceHost();
            instantiateService();
            host.Faulted += host_Faulted;
        }

        protected virtual void initializeServiceHost()
        {
            host = new System.ServiceModel.ServiceHost(typeof(Server), Endpoint);
        }

        private void instantiateService()
        {
            try
            {
                ServiceMetadataBehavior smbBehavior = host.Description.Behaviors.Find<ServiceMetadataBehavior>();
                if (smbBehavior == null)
                {
                    smbBehavior = new ServiceMetadataBehavior();
                    host.Description.Behaviors.Add(smbBehavior);
                }

                ServiceThrottlingBehavior throttleBehavior = host.Description.Behaviors.Find<ServiceThrottlingBehavior>();
                if (throttleBehavior == null)
                {
                    throttleBehavior = new ServiceThrottlingBehavior();
                };

                throttleBehavior.MaxConcurrentCalls = 120;
                throttleBehavior.MaxConcurrentInstances = 500;
                throttleBehavior.MaxConcurrentSessions = 500;
                host.Description.Behaviors.Add(throttleBehavior);

                ServiceDebugBehavior debugBehavior = host.Description.Behaviors.Find<ServiceDebugBehavior>();
                if (debugBehavior == null)
                {
                    debugBehavior = new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true };
                    host.Description.Behaviors.Add(debugBehavior);
                }
                else
                {
                    if (!debugBehavior.IncludeExceptionDetailInFaults)
                        debugBehavior.IncludeExceptionDetailInFaults = true;
                }
          
                NetTcpBinding binding = new NetTcpBinding();
                binding.Security.Mode = SecurityMode.None;
                binding.ReliableSession.Enabled = false;
                binding.ReliableSession.Ordered = true;
                binding.MaxConnections = int.MaxValue;
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.MaxBufferSize = int.MaxValue;
                
                //binding.MaxReceivedMessageSize = 100000;
                //binding.SendTimeout = new TimeSpan(0, 0, 30);
                //binding.ReceiveTimeout = new TimeSpan(0, 0, 30);

                host.AddServiceEndpoint(getServerType(), binding, "");
                host.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName, binding, "mex");
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("On instantiateService()", this, ex);
            }
        }

        protected virtual Type getServerType()
        {
           return typeof(IServer);
        }

        private void startWCFListener()
        {
            try
            {
                SessionManager.Instance.Communication = this;

                host.Open();
            }
            catch(AddressAlreadyInUseException aaiue)
            {
                Log.Instance.WriteError("On startWCFListener()", this, aaiue);
            }
            catch (CommunicationException ce)
            {
                Log.Instance.WriteError("On startWCFListener()", this, ce);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On startWCFListener()", this, ex);
            }
        }

        void host_Faulted(object sender, EventArgs e)
        {
            Log.Instance.Write("on host_Faulted, restarting wcf listener", this);
            startWCFListener();
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                deviceIds.Clear();
                deviceIds = null;

                try
                {
                    host.Close();
                }
                catch { }
            }
        }

        #endregion
    }
}