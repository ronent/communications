﻿using Infrastructure.Communication;
using Infrastructure.Communication.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFCommunication.Interfaces
{
    [ServiceContract(CallbackContract = typeof(IClient))]
    public interface IServer
    {
        /// <summary>
        /// Opens the session.
        /// </summary>
        [OperationContract]
        bool OpenSession();

        /// <summary>
        /// Keeps the session alive.
        /// </summary>
        [OperationContract]
        bool KeepAlive();

        /// <summary>
        /// Occurs when device connected.
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void OnDeviceConnected(ConnectionEventArgs args);

        /// <summary>
        /// Occurs when device removed.
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void OnDeviceRemoved(ConnectionEventArgs args);

        /// <summary>
        /// Occurs when device send data from client to server.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="data">The data.</param>
        [OperationContract(IsOneWay = true)]        
        void OnDataReceived(string id, byte[] data);

        /// <summary>
        /// Occurs when device send bundle data from client to server.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="data">The data.</param>
        [OperationContract(IsOneWay = true)]
        void OnBundleReceived(string id, IEnumerable<byte[]> data);
    }
}
