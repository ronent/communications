﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace HidLibrary
{
    internal class HidDeviceEventMonitor
    {
        public event InsertedEventHandler Inserted;
        public event RemovedEventHandler Removed;

        public delegate void InsertedEventHandler();
        public delegate void RemovedEventHandler();

        private readonly HidDevice _device;
        private bool _wasConnected;

        public HidDeviceEventMonitor(HidDevice device)
        {
            _device = device;
        }

        public void Init()
        {
            var eventMonitor = new Action(DeviceEventMonitor);
            eventMonitor.BeginInvoke(DisposeDeviceEventMonitor, eventMonitor);
        }

        private void DeviceEventMonitor()
        {
            var isConnected = _device.IsConnected;

            if (isConnected != _wasConnected)
            {
                if (isConnected && Inserted != null)
                    Inserted();
                else if (!isConnected && Removed != null)
                    Removed();

                _wasConnected = isConnected;
            }

            if (_device.MonitorDeviceEvents)
            {
                TaskDelay.Delay(1000).ContinueWith((result) =>
                {
                    Init();
                });
            }
        }

        private static void DisposeDeviceEventMonitor(IAsyncResult ar)
        {
            try
            {
                ((Action)ar.AsyncState).EndInvoke(ar);
            }
            catch { }
        }
    }
}
