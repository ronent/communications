﻿using Auxiliary.Tools;
using HidLibrary;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.Communication.Modules;
using Infrastructure.Communication.Modules.HID;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HidCommunication
{
    public delegate void DataReceivedDelegate(byte[] data);

    [Export(typeof(ICommunicationManager))]
    [ExportMetadata("Name", "HID Communication")]
    public class CommunicationManager : IHIDCommunicationManager
    {
        #region Fields
        private ConcurrentBag<DeviceID> listdeviceIDs;
        private ConcurrentDictionary<string, HidDevice> connectedDevices;
        private volatile bool scanForDevices;
        private static TimeSpan scanningWaiter = new TimeSpan(0, 0, 0, 0, 250);

        #endregion
        #region Constructor
        public CommunicationManager()
        {
            IsDisposed = false;
            listdeviceIDs = new ConcurrentBag<DeviceID>();
            connectedDevices = new ConcurrentDictionary<string, HidDevice>();

            scanForDevices = false;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        #endregion
        #region Methods
        void device_Inserted(HidDevice device)
        {
            Task.Factory.StartNew(() =>
                {
                    deviceConnected(device);
                }, TaskCreationOptions.AttachedToParent);
        }

        private void deviceConnected(HidDevice device)
        {
            if (connectedDevices.TryAdd(device.DevicePath, device))
            {
                ConnectionEventArgs args = new HIDConnectionEventArgs()
                {
                    DeviceID = new DeviceID((uint)device.Attributes.VendorId, (uint)device.Attributes.ProductId),
                    UID = device.DevicePath,
                    DeviceIO = new DeviceIO(this, device),
                };

                (args.DeviceIO as DeviceIO).ListenForData();

                Log.Instance.Write("[HID Communication] ==> Device connected: '{0}'", this, Log.LogSeverity.DEBUG, args.DeviceIO.ShortId);
                
                Utilities.InvokeEvent(OnDeviceConnected, this, args);
            }
        }

        internal void device_Removed(HidDevice device)
        {
            Task.Factory.StartNew(() =>
            {
                deviceRemoved(device);
            }, TaskCreationOptions.AttachedToParent);
        }

        private void deviceRemoved(HidDevice device)
        {
            HidDevice removedDevice;
            if (connectedDevices.TryRemove(device.DevicePath, out removedDevice))
            {
                try
                {
                    ConnectionEventArgs args = new HIDConnectionEventArgs()
                    {
                        DeviceID = new DeviceID((uint)device.Attributes.VendorId, (uint)device.Attributes.ProductId),
                        UID = device.DevicePath,
                        DeviceIO = new DeviceIO(this, device),
                    };

                    removedDevice.Inserted -= device_Inserted;
                    removedDevice.Removed -= device_Removed;

                    Log.Instance.Write("[HID Communication] ==> Device disconnected: '{0}'", this, Log.LogSeverity.DEBUG, args.DeviceIO.ShortId);

                    Utilities.InvokeEvent(OnDeviceRemoved, this, args);
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
            }
        }

        #endregion
        #region ICommunicationManager
        public event DeviceConnectedDelegate OnDeviceConnected;
        public event DeviceRemovedDelegate OnDeviceRemoved;

        public List<DeviceID> Ids
        {
            get { return listdeviceIDs.ToList(); }
        }

        public void RegisterDeviceIds(List<DeviceID> deviceIDs)
        {
            throwIfDisposed();

            Parallel.ForEach(deviceIDs, item => listdeviceIDs.Add(item));
            listdeviceIDs = new ConcurrentBag<DeviceID>(listdeviceIDs.Distinct());
        }

        public void UnregisterDeviceIds()
        {
            throwIfDisposed();

            Shutdown();
            listdeviceIDs = new ConcurrentBag<DeviceID>();
        }

        private void disconnectAllDevices()
        {
            var enu = connectedDevices.GetEnumerator();
            while (enu.MoveNext())
            {
                try
                {
                    enu.Current.Value.Dispose();
                }
                catch { }
            }

            connectedDevices.Clear();
        }

        public void ScanForDevices()
        {
            throwIfDisposed();

            scanForDevices = true;

            Log.Instance.Write("[HID Communication] ==> Started scanning for devices", this, Log.LogSeverity.DEBUG);

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        while (!IsDisposed && scanForDevices)
                        {
                            var enu = listdeviceIDs.GetEnumerator();

                            while (!IsDisposed && enu.MoveNext() && scanForDevices)
                            {
                                var hid = (HidDevices.Enumerate((int)enu.Current.VID, (int)enu.Current.PID)).GetEnumerator();
                                while (!IsDisposed && hid.MoveNext() && scanForDevices)
                                {
                                    HidDevice outDevice;
                                    if (!connectedDevices.TryGetValue(hid.Current.DevicePath, out outDevice))
                                    {
                                        HidDevice device = hid.Current;

                                        device.MonitorDeviceEvents = true;
                                        device.Inserted += device_Inserted;
                                        device.Removed += device_Removed;
                                    }
                                }
                            }

                            TaskDelay.Delay(scanningWaiter).Wait();
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.WriteError("On ScanForDevices()", this, ex);
                        throw (ex);
                    }
                }, TaskCreationOptions.LongRunning);
        }

        public void Shutdown()
        {
            throwIfDisposed();

            scanForDevices = false;
            disconnectAllDevices();
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                scanForDevices = false;
                UnregisterDeviceIds();

                IsDisposed = true;
                OnDeviceConnected = null;
                OnDeviceRemoved = null;
            }
        }

        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
    }
}