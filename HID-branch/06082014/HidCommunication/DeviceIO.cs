﻿using Auxiliary.Tools;
using HidLibrary;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace HidCommunication
{
    public class DeviceIO : IDeviceIO, IDisposable
    {
        #region Fields
        public event DataReceivedEventHandler OnDataReceived;
        public event DataSentEventHandler OnDataSent;

        private HidLibrary.HidDevice deviceIO;
        private CommunicationManager parent;
        private bool writeToLog = false;

        #endregion
        #region Constructor
        public DeviceIO(CommunicationManager parent, HidLibrary.HidDevice deviceIO)
        {
            IsDisposed = false;

            this.deviceIO = deviceIO;
            this.parent = parent;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        #endregion
        #region IDeviceIO
        public string Id
        {
            get { return deviceIO.DevicePath; } // + this.GetHashCode()
        }

        public string ShortId
        {
            get { return Id.Split('&')[2]; }
        }

        public void ListenForData()
        {
            deviceIO.OpenDevice();

            Task.Factory.StartNew(() =>
            {
                while (!IsDisposed && deviceIO.IsOpen)
                {
                    HidLibrary.HidDeviceData data = deviceIO.Read();
                    if (data.Data != null && data.Data.Length > 0 && data.Status == HidLibrary.HidDeviceData.ReadStatus.Success)
                    {
                        Utilities.InvokeEvent(OnDataReceived, this, new DataTransmittedEventArgs(data.Data));

                        if (writeToLog)
                            Log.Instance.Write("[HID Communication] ==> Device '{0}' received opcode '{1}' from device, Thread #{2}.", this, Log.LogSeverity.DEBUG, ShortId, data.Data[1].ToString("X2"), Thread.CurrentThread.ManagedThreadId);
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        public void SendData(byte[] data)
        {
            throwIfDisposed();

            deviceIO.Write(data);
            Utilities.InvokeEvent(OnDataSent, this, new DataTransmittedEventArgs(data));

            if (writeToLog)
                Log.Instance.Write("[HID Communication] ==> Device '{0}' sending opcode '{1}' to device, Thread #{2}.", this, Log.LogSeverity.DEBUG, ShortId, data[1].ToString("X2"), Thread.CurrentThread.ManagedThreadId);
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                parent.device_Removed(deviceIO);
                deviceIO.Dispose();

                deviceIO = null;
                OnDataReceived = null;
                OnDataSent = null;
                parent = null;
            }
        }

        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
    }
}