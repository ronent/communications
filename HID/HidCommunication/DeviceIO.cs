﻿using Auxiliary.Tools;
using HidLibrary;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.Communication.Modules;
using Infrastructure.Communication.Modules.HID;
using Infrastructure.FunctionQueue;
using Log4Tech;
using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace HidCommunication
{
    public class DeviceIO : IHIDDeviceIO, IDisposable
    {
        #region Fields
        public event DataReceivedEventHandler OnDataReceived;
        public event DataSentEventHandler OnDataSent;
        public Action OnOffline { get; set; }

        internal HidLibrary.HidDevice deviceIO;
        private CommunicationManager parent;
        private bool writeToLog = false;

        private string id;

        #endregion
        #region Constructor
        public DeviceIO(CommunicationManager parent, HidLibrary.HidDevice deviceIO)
        {
            IsDisposed = false;

            this.deviceIO = deviceIO;
            this.parent = parent;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        #endregion
        #region IDeviceIO
        public string Id
        {
            get
            {
                if (id == null)
                    id = deviceIO.DevicePath;

                return id;
            }
        }

        public string ShortId
        {
            get { return Id.Split('&')[2]; }
        }

        public void ListenForData()
        {
            deviceIO.OpenDevice();

            Task.Factory.StartNew(() =>
            {
                while (!IsDisposed && deviceIO.IsOpen)
                {
                    var data = deviceIO.ReadReport();
                    if (data.ReadStatus == HidDeviceData.ReadStatus.Success && data.Exists)
                    {
                        byte[] newData = new byte[65];
                        Array.ConstrainedCopy(data.Data, 0, newData, 1, data.Data.Length);

                        Utilities.InvokeEvent(OnDataReceived, this, new DataTransmittedEventArgs(newData));

                        if (writeToLog)
                            Log.Instance.Write("[HID Communication] ==> Device '{0}' received opcode '{1}' from device, Thread #{2}.", this, Log.LogSeverity.DEBUG, ShortId, newData[1].ToString("X2"), Thread.CurrentThread.ManagedThreadId);
                    }
                    
                }
            }, TaskCreationOptions.LongRunning);
        }

        public void SendData(byte[] data)
        {
            throwIfDisposed();

            deviceIO.Write(data);
            Utilities.InvokeEvent(OnDataSent, this, new DataTransmittedEventArgs(data));

            if (writeToLog)
                Log.Instance.Write("[HID Communication] ==> Device '{0}' sending opcode '{1}' to device, Thread #{2}.", this, Log.LogSeverity.DEBUG, ShortId, data[1].ToString("X2"), Thread.CurrentThread.ManagedThreadId);
        }

        #endregion
        #region IHIDDeviceIO
        private FunctionQueue functionQueue;

        public FunctionQueue FunctionQueue
        {
            get
            {
                if (functionQueue == null || (parent != null && parent.FunctionQueue != functionQueue))
                    functionQueue = parent.FunctionQueue;

                return functionQueue;
            }
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                parent.device_Removed(deviceIO);
                deviceIO.Dispose();

                deviceIO = null;
                OnDataReceived = null;
                OnDataSent = null;
                parent = null;

                if (OnOffline != null)
                    OnOffline();
            }
        }

        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
    }
}