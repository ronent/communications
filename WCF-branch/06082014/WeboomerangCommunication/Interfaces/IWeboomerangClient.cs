﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WCFCommunication.Interfaces;

namespace WeboomerangCommunication.Interfaces
{
    public interface IWeboomerangClient : IClient
    {
        [OperationContract(IsOneWay = true)]
        void OnDeviceIdentification(string id, string serialNumber, string comment);

        [OperationContract(IsOneWay = true)]
        void OnStatusString(string id, string status, bool isError);

        [OperationContract(IsOneWay = true)]
        void OnProgressReport(string id, int percents);

        [OperationContract(IsOneWay = true)]
        void OnBoomerangReport(string id, string uri, string status, string finishStatus);
    }
}
