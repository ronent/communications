﻿using Infrastructure.Communication;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WeboomerangCommunication.Interfaces;

namespace WeboomerangCommunication
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, AddressFilterMode = AddressFilterMode.Any, InstanceContextMode = InstanceContextMode.PerSession, MaxItemsInObjectGraph = int.MaxValue)]
    public class Server : WCFCommunication.Server, WCFCommunication.Interfaces.IServer, IWeboomerangServer
    {
        #region Fields
        private bool writeToLog = false;

        #endregion
        #region Constructor
        public Server()
            :base()
        {

        }

        #endregion
        #region Properties
        private IWeboomerangClient client { get { return base.client as IWeboomerangClient; } set { base.client = value; } }

        #endregion
        #region Override Methods
        protected override void getCallbackChannel()
        {
            client = OperationContext.Current.GetCallbackChannel<IWeboomerangClient>();
        }

        protected override WCFCommunication.DeviceIO generateDeviceIO(ConnectionEventArgs args)
        {
            return new DeviceIO(this, args.UID);
        }

        #endregion
        #region Methods
        protected DeviceIO GetDevice(string devicePath)
        {
            try
            {
                return clientDevices[devicePath] as DeviceIO;
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On GetDevice()", this, ex);
            }

            return null;
        }

        public void InvokeOnDeviceIdentification(string id, string serialNumber, string comment)
        {
            try
            {
                client.OnDeviceIdentification(id, serialNumber, comment);
            }
            catch (Exception ex)
            {
                if (writeToLog)
                    Log4Tech.Log.Instance.WriteError("On InvokeOnDeviceIdentification()", this, ex);
            }
        }

        public void InvokeOnStatusString(string id, string status, bool isError)
        {
            try
            {
                client.OnStatusString(id, status, isError);
            }
            catch (Exception ex) 
            {
                if (writeToLog)
                    Log4Tech.Log.Instance.WriteError("On InvokeOnStatusString()", this, ex);
            }
        }

        public void InvokeOnProgressReport(string id, int percents)
        {
            try
            {
                client.OnProgressReport(id, percents);
            }
            catch (Exception ex)
            {
                if (writeToLog)
                    Log4Tech.Log.Instance.WriteError("On InvokeOnProgressReport()", this, ex);
            }
        }

        public void InvokeOnBoomerangReport(string id, string uri, string status, string finishStatus)
        {
            try
            {
                client.OnBoomerangReport(id, uri, status, finishStatus);
            }
            catch (Exception ex)
            {
                if (writeToLog)
                    Log4Tech.Log.Instance.WriteError("On InvokeOnBoomerangReport()", this, ex);
            }
        }

        #endregion
    }
}