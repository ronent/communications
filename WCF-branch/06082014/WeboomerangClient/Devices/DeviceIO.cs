﻿using Auxiliary.Tools;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WCFClient.Communications.EventArgs;
using WCFClient.Devices;
using WeboomerangClient.Communications;
using WeboomerangClient.Communications.EventArgs;

namespace WeboomerangClient.Devices
{
    public delegate void DeviceIdentificationDelegate(DeviceIdentificationEventArgs args);
    public delegate void NewStatusDelegate(string status);
    public delegate void DownloadReportDelegate(DownloadReportEventArgs args);
    public delegate void ProgressReportDelegate(int percents);

    public class DeviceIO : WCFClient.Devices.DeviceIO
    {
        #region Fields

        #endregion
        #region Events
        public event DeviceIdentificationDelegate OnDeviceIdentification;
        public event NewStatusDelegate OnNewStatus;
        public event NewStatusDelegate OnNewErrorStatus;
        public event DownloadReportDelegate OnDownloadReport;
        public event ProgressReportDelegate OnProgressReport;

        #endregion
        #region Constructor
        public DeviceIO(ServerAPI communicationManager, IDeviceIO deviceIO)
            :base(communicationManager, deviceIO)
        {

        }

        #endregion
        #region Properties
        public override string Name
        {
            get
            {
                if (SerialNumber == null)
                    return deviceIO.ShortId;
                else
                    return string.Format("#{0}", SerialNumber);
            }
        }

        public string SerialNumber { get; private set; }
        public string Comment { get; private set; }

        public Uri ReportAdress { get; private set; }

        #endregion
        #region Methods
        internal void DeviceIdentification(string serialNumber, string comment)
        {
            SerialNumber = serialNumber;
            Comment = comment;

            Log.Instance.Write("[Weboomerang Client] ==> Device '{0}' is S/N: {1}", this, Log.LogSeverity.DEBUG, DeviceName, Name);

            if (OnDeviceIdentification != null)
                OnDeviceIdentification.BeginInvoke(new DeviceIdentificationEventArgs(serialNumber, comment), null, null);
        }

        internal void StatusString(string status, bool isError)
        {
            invokeNewStatus(status, isError);
        }

        internal void ProgressReport(int percents)
        {
            if (OnProgressReport != null)
                OnProgressReport.BeginInvoke(percents, null, null);
        }

        internal void BoomerangReport(string uri, string status, string finishStatus)
        {
            Log.Instance.Write("[Weboomerang Client] ==> Device '{0}' report URI: '{1}', Status: '{2}', Finish Status: '{3}'", this, Log.LogSeverity.DEBUG, Name, uri, status, finishStatus);

            invokeNewStatus(status);

            ReportAdress = new Uri(uri);

            if (OnDownloadReport != null)
                OnDownloadReport.BeginInvoke(new DownloadReportEventArgs(ReportAdress, finishStatus), null, null);
        }

        private void invokeNewStatus(string status, bool isError = false)
        {
            if (isError)
            {
                if (OnNewErrorStatus != null)
                    OnNewErrorStatus.BeginInvoke(status, null, null);
            }
            else
            {
                if (OnNewStatus != null)
                    OnNewStatus.BeginInvoke(status, null, null);
            }
        }

        protected override void OnErrorStatus(object sender, StatusEventArgs args)
        {
            base.OnErrorStatus(sender, args);
        }
        
        #endregion
    }
}
