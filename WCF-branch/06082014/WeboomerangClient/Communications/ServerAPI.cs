﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WeboomerangClient.Service;

namespace WeboomerangClient.Communications
{
    public class ServerAPI : WCFClient.Communications.ServerAPI
    {
        #region Constructor
        public ServerAPI(CommunicationManager communicationManager)
            : base(communicationManager)
        {
            parent = communicationManager;
        }

        #endregion
        #region Properties
        protected CommunicationManager parent { get { return base.parent as CommunicationManager; } set { base.parent = value; } }
        protected ClientAPI client { get { return base.client as ClientAPI; } }

        #endregion
        #region Override Methods
        protected override void initializeServer()
        {
            service = new ServiceManager(parent.ServerURI, client);
        }

        #endregion
    }
}