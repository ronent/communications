﻿using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WeboomerangClient.Devices;

namespace WeboomerangClient.Communications
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public class ClientAPI : WCFClient.Communications.ClientAPI, WCFClient.ServiceReference.IServerCallback, ServiceReference.IWeboomerangServerCallback
    {
        #region Constructor
        public ClientAPI(CommunicationManager communicationManager)
            : base(communicationManager)
        {

        }

        #endregion
        #region Properties
        protected CommunicationManager parent { get { return base.parent as CommunicationManager; } set { base.parent = value; } }
        private DeviceManager deviceManager { get { return parent.deviceManager as DeviceManager; } }

        #endregion
        #region IWeboomerangClient
        public void OnDeviceIdentification(string id, string serialNumber, string comment)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.DeviceIdentification(serialNumber, comment);
        }

        public void OnStatusString(string id, string status, bool isError)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.StatusString(status, isError);
        }

        public void OnProgressReport(string id, int percents)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.ProgressReport(percents);
        }

        public void OnBoomerangReport(string id, string uri, string status, string finishStatus)
        {
            DeviceIO device = deviceManager.GetDevice(id);
            if (device != null)
                device.BoomerangReport(uri, status, finishStatus);
        }

        #endregion
    }
}