﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeboomerangClient.Communications.EventArgs
{
    public class DeviceIdentificationEventArgs : System.EventArgs
    {
        public string SerialNumber { get; private set; }
        public string Comment { get; private set; }

        public DeviceIdentificationEventArgs(string serialNumber, string comment)
            :base()
        {
            SerialNumber = serialNumber;
            Comment = comment;
        }
    }
}
