﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeboomerangClient.Communications.EventArgs
{
    public class DownloadReportEventArgs : System.EventArgs
    {
        public Uri ReportUri { get; private set; }
        public string Status { get; private set; }

        public DownloadReportEventArgs(Uri report, string status)
            :base()
        {
            ReportUri = report;
            Status = status;
        }
    }
}
