﻿using Infrastructure.Communication;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFCommunication
{
    public class SessionManager
    {
        private ConcurrentDictionary<string, Server> clientSockets;

        private SessionManager() 
        {
            clientSockets = new ConcurrentDictionary<string, Server>();
        }

        private static readonly Lazy<SessionManager> lazy = new Lazy<SessionManager>(() => new SessionManager());

        public static SessionManager Instance 
         { 
             get { return lazy.Value; } 
         }

        public CommunicationManager Communication { get; set; }

        public void AddNewSession(Server session)
        {
            try
            { 
                clientSockets.AddOrUpdate(session.SessionID, session, (key, oldSession) => session);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On AddNewSession()", this, ex);
            }
        }

        public void removeSession(string id)
        {
            try
            { 
                Server removedSession;
                if (clientSockets.TryRemove(id, out removedSession))
                    removedSession.Dispose();
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On removeSession()", this, ex);
            }
        }

        internal void InvokeOnDeviceConnected(ConnectionEventArgs args)
        {
            try
            {
                Communication.InvokeOnDeviceConnected(this, args as HIDConnectionEventArgs);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On InvokeOnDeviceConnected()", this, ex);
            }
        }

        internal void InvokeOnDeviceDisonnected(ConnectionEventArgs args)
        {
            try
            {
                Communication.InvokeOnDeviceDisonnected(this, args);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On InvokeOnDeviceDisonnected()", this, ex);
            }
        }
    }
}
