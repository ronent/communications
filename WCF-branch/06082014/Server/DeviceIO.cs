﻿using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFCommunication
{
    public class DeviceIO : IDisposable, IDeviceIO, IBundle, IDownloadProgress
    {
        #region Members
        private bool writeToLog = false;
        protected Server parent;

        #endregion
        #region Constructor
        public DeviceIO(Server server, string id)
        {
            IsDisposed = false;
            Id = id;
            parent = server;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        #endregion
        #region IDeviceIO
        public event DataReceivedEventHandler OnDataReceived;
        public event DataSentEventHandler OnDataSent;

        public string Id
        {
            get;
            private set;
        }

        public string ShortId
        {
            get { return Id.Split('&')[2]; }
        }

        public void SendData(byte[] data)
        {
            throwIfDisposed();

            parent.OnDataSend(Id, data);

            Utilities.InvokeEvent(OnDataSent, this, new DataTransmittedEventArgs(data));
        }

        public void InvokeReceiveData(byte[] data)
        {
            throwIfDisposed();

            Utilities.InvokeEvent(OnDataReceived, this, new DataTransmittedEventArgs(data));
        }

        #endregion
        #region IDownloadProgress
        public void InvokeDownloadProgress(int percents)
        {
            throwIfDisposed();

            parent.InvokeDownloadProgress(Id, percents);
        }

        #endregion
        #region IBundle
        public event DataReceivedEventHandler OnBundleReceived;

        public void InvokeLoopThroughAsLong(LoopArgs args)
        {
            parent.InvokeLoopThroughAsLong(Id, args);
        }

        public bool IsBundleSupported
        {
            get { return true; }
        }

        public void InvokeReceiveBundle(IEnumerable<byte[]> data)
        {
            Utilities.InvokeEvent(OnBundleReceived, this, new DataTransmittedEventArgs(data));
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                if (writeToLog)
                    Log.Instance.Write("[WCF Communication] ==> Device '{0}' is disposing", this, Log.LogSeverity.DEBUG, ShortId);

                IsDisposed = true;

                parent.OnDeviceRemoved(new ConnectionEventArgs() { UID = Id });
                parent = null;

                OnDataReceived = null;
                OnDataSent = null;
                OnBundleReceived = null;
            }
        }

        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
    }
}
