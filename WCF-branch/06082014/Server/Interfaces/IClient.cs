﻿using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFCommunication.Interfaces
{
    public interface IClient
    {
        /// <summary>
        /// Scans for devices.
        /// </summary>
        [OperationContract(IsOneWay=true)]        
        void ScanForDevices();

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        [OperationContract(IsOneWay = true)]        
        void Shutdown();

        /// <summary>
        /// Registers the HID device ids.
        /// </summary>
        /// <param name="deviceIDs">The device ids.</param>
        [OperationContract(IsOneWay = true)]                
        void RegisterDeviceIds(List<DeviceID> deviceIDs);

        /// <summary>
        /// Unregisters the HID device ids.
        /// </summary>
        [OperationContract(IsOneWay = true)]                
        void UnregisterDeviceIds();

        /// <summary>
        /// Occurs when device send data from server to client.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="data">The data.</param>
        [OperationContract(IsOneWay = true)]
        void OnDataSend(string id, byte[] data);

        /// <summary>
        /// Occurs when device downloads data.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="percents">The progress.</param>
        [OperationContract(IsOneWay = true)]
        void OnDownloadProgress(string id, int percents);

        /// <summary>
        /// Invokes the loop through mechanism.
        /// </summary>
        /// <param name="args">The arguments.</param>
        [OperationContract(IsOneWay = true)]
        void InvokeLoopThroughAsLong(string id, LoopArgs args);

        /// <summary>
        /// Called when [ignore op codes].
        /// </summary>
        /// <param name="opCodeStartByte">The op code start byte.</param>
        /// <param name="opCodesToIgnore">The op codes to ignore.</param>
        [OperationContract(IsOneWay = true)]
        void OnIgnoreOpCodes(string id, int opCodeStartByte, IEnumerable<byte[]> opCodesToIgnore);
    }
}
