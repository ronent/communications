﻿using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WCFCommunication.Interfaces;

namespace WCFCommunication
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, AddressFilterMode = AddressFilterMode.Any, 
                    InstanceContextMode = InstanceContextMode.PerSession, MaxItemsInObjectGraph = int.MaxValue)]
    public class Server : IServer, IDisposable
    {
        #region Members
        private static bool writeToLog = false;
        private const int MAXIMUM_DOWNLOADS = 1;

        protected IClient client;
        protected ConcurrentDictionary<string, DeviceIO> clientDevices;

        #endregion
        #region Constructor
        public Server()
        {
            IsDisposed = false;

            SessionManager.Instance.AddNewSession(this);
            clientDevices = new ConcurrentDictionary<string, DeviceIO>();

            try
            {
                getCallbackChannel();
                OperationContext.Current.Channel.Faulted += Channel_Faulted;
                OperationContext.Current.Channel.Closed += Channel_Faulted;

                client.RegisterDeviceIds(SessionManager.Instance.Communication.Ids);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On Server()", this, ex);
            }
        }

        protected virtual void getCallbackChannel()
        {
            client = OperationContext.Current.GetCallbackChannel<IClient>();
        }

        #endregion
        #region Properties
        private string sessionID;
        public string SessionID
        {
            get 
            {
                if (sessionID == null)
                    sessionID = OperationContext.Current.SessionId;

                return sessionID;
            }
        }

        public bool IsDisposed { get; private set; }

        public int NumberOfConnectedDevices { get { return clientDevices.Count; } }

        #endregion
        #region IServer
        public bool OpenSession()
        {
            return true;
        }

        public bool KeepAlive()
        {
            return true;
        }

        public void OnDeviceConnected(ConnectionEventArgs args)
        {
            throwIfDisposed();

            Task.Factory.StartNew(() => 
            {
                try
                {
                    DeviceIO deviceIO = generateDeviceIO(args);
                    args.DeviceIO = deviceIO;
                    clientDevices.AddOrUpdate(args.UID, deviceIO, (key, oldDevice) => deviceIO);

                    SessionManager.Instance.InvokeOnDeviceConnected(args);
                }
                catch (Exception ex)
                {
                    if(writeToLog)
                        Log.Instance.WriteError("On OnDeviceConnected()", this, ex);
                }
            }, TaskCreationOptions.PreferFairness);
        }

        protected virtual DeviceIO generateDeviceIO(ConnectionEventArgs args)
        {
            return new DeviceIO(this, args.UID);
        }

        public void OnDeviceRemoved(ConnectionEventArgs args)
        {
            throwIfDisposed();

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        DeviceIO removedDevice;
                        if (clientDevices.TryRemove(args.UID, out removedDevice))
                        {
                            args.DeviceIO = removedDevice;

                            SessionManager.Instance.InvokeOnDeviceDisonnected(args);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (writeToLog)
                            Log.Instance.WriteError("On OnDeviceRemoved()", this, ex);
                    }
                }, TaskCreationOptions.PreferFairness);
        }

        public void OnDataReceived(string id, byte[] data)
        {
            throwIfDisposed();

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        clientDevices[id].InvokeReceiveData(data);
                    }
                    catch (Exception ex)
                    {
                        if (writeToLog)
                            Log.Instance.WriteError("On OnDataReceived()", this, ex);
                    }
                }, TaskCreationOptions.AttachedToParent);
        }

        public void OnDataSend(string id, byte[] data)
        {
            throwIfDisposed();

            try
            {
                client.OnDataSend(id, data);
            }
            catch (Exception ex)
            {
                if (writeToLog)
                    Log.Instance.WriteError("On OnDataSend()", this, ex);
            }
        }

        public void InvokeLoopThroughAsLong(string id, LoopArgs args)
        {
            throwIfDisposed();

            try
            {
                client.InvokeLoopThroughAsLong(id, args);
            }
            catch (Exception ex)
            {
                if (writeToLog)
                    Log.Instance.WriteError("On InvokeLoopThroughAsLong()", this, ex);
            }
        }

        public void InvokeDownloadProgress(string id, int percents)
        {
            throwIfDisposed();

            try
            {
                client.OnDownloadProgress(id, percents);
            }
            catch (Exception ex)
            {
                if (writeToLog)
                    Log.Instance.WriteError("On InvokeDownloadProgress()", this, ex);
            }
        }

        public void OnBundleReceived(string id, IEnumerable<byte[]> data)
        {
            throwIfDisposed();

            Task.Factory.StartNew(() =>
            {
                try
                {
                    clientDevices[id].InvokeReceiveBundle(data);
                }
                catch (Exception ex)
                {
                    if (writeToLog)
                        Log.Instance.WriteError("On OnBundleReceived()", this, ex);
                }
            }, TaskCreationOptions.AttachedToParent);
        }

        void Channel_Faulted(object sender, EventArgs e)
        {
            handleDisconnection();
        }

        private void handleDisconnection()
        {
            SessionManager.Instance.removeSession(SessionID);
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {

                IsDisposed = true;

                try
                {
                    foreach (var item in clientDevices)
                        item.Value.Dispose();

                    client = null;

                    if (OperationContext.Current != null &&
                            OperationContext.Current.Channel.State != CommunicationState.Closed &&
                            OperationContext.Current.Channel.State != CommunicationState.Closing && 
                            OperationContext.Current.Channel.State != CommunicationState.Faulted)

                        OperationContext.Current.Channel.Close();
                }
                catch (Exception ex)
                {
                    if (writeToLog)
                        Log.Instance.WriteError("On Dispose()", this, ex);
                }
            }
        }

        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
    }
}