﻿using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WCFClient.Communications;
using WCFClient.Communications.EventArgs;
using WCFClient.Devices.Optimizations;
using WCFClient.Utilities;

namespace WCFClient.Devices
{
    public class DeviceIO : IDisposable
    {
        #region Members
        private bool writeToLog = false;
        private ServerAPI parent;
        private LoopThroughAsLongAggregate loop;

        protected internal IDeviceIO deviceIO;
        protected internal Optimizations.IgnoredOpCodes IgnoredOpCodes { get; private set; }
        private volatile bool funnel = false;

        #endregion
        #region Events
        public event ProgressReportDelegate OnDownloadProgressReport;
        public event Action OnDownloading;
        public event EventHandler<StatusEventArgs> ErrorStatus;

        public event DataReceivedEventHandler OnDataReceived
        {
            add { deviceIO.OnDataReceived += value; }
            remove { deviceIO.OnDataReceived -= value; }
        }

        public event DataSentEventHandler OnDataSent
        {
            add { deviceIO.OnDataSent += value; }
            remove { deviceIO.OnDataSent -= value; }
        }

        #endregion
        #region Constructor
        public DeviceIO(ServerAPI serverAPI, IDeviceIO deviceIO)
        {
            parent = serverAPI;
            this.deviceIO = deviceIO;
            IsDisposed = false;
            Name = DeviceName;

            initialize();
            registerIOEvents();
        }

        private void initialize()
        {
            IgnoredOpCodes = new Optimizations.IgnoredOpCodes();
        }

        #endregion
        #region Properties
        public virtual string Name { get; private set; }

        public string Id { get { return deviceIO.Id; } }

        public bool IsDisposed { get; private set; }

        public bool IsConnected { get { return parent.IsConnected; } }

        #endregion
        #region Protected events
        protected virtual void OnErrorStatus(object sender, StatusEventArgs args)
        {
            if (ErrorStatus != null)
                ErrorStatus(sender,args);
        }

        #endregion
        #region Methods
        private void registerIOEvents()
        {
            funnel = true;
            deviceIO.OnDataReceived += deviceIO_OnDataReceived;
        }

        private void unregisterIOEvents()
        {
            funnel = false;
            deviceIO.OnDataReceived -= deviceIO_OnDataReceived;
        }

        void deviceIO_OnDataReceived(object sender, DataTransmittedEventArgs e)
        {
            dataReceived(e.Data);
        }

        private void dataReceived(byte[] data)
        {
            try
            {
                if (IsConnected && funnel && !IgnoredOpCodes.Contains(data) && data[1] != 0)
                {
                    if (writeToLog)
                        Log.Instance.Write("[WCF Client] ==> Device '{0}' From: {1}", this, Log.LogSeverity.DEBUG, Name, data[1].ToString("X2"));

                    parent.SendData(deviceIO.Id, data);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[WCF Client] ==> On deviceIO_OnDataReceived()", this, ex);
            }
        }

        internal void SendData(byte[] data)
        {
            if (IsConnected)
            {
                if (writeToLog)
                    Log.Instance.Write("[WCF Client] ==> Device '{0}' To: {1}", this, Log.LogSeverity.DEBUG, Name, data[1].ToString("X2"));

                deviceIO.SendData(data);
            }
        }

        internal void InvokeDownloadProgress(int percents)
        {
            downloadProgressReport(percents);            
        }

        internal void InvokeLoopThroughAsLong(LoopArgs args)
        {
            unregisterIOEvents();
            loop = new LoopThroughAsLongAggregate(this, args);

            loop.OnProgressReport += (sender, percent) =>
            {
                downloadProgressReport(percent);
            };

            loop.OnBundle += (sender, e) =>
                {
                    parent.OnBundleReceived(Id, e.Packets);
                };

            loop.OnFinished += (sender, e) =>
            {
                registerIOEvents();
                if (e.Packets.Count > 0)
                    parent.OnBundleReceived(Id, e.Packets);
            };

            loop.OnDownloading += () =>
            {
                if (OnDownloading != null)
                    OnDownloading();
            };

            loop.Start();
        }

        internal void SetIgnoreOpCodes(int opCodeStartByte, IEnumerable<byte[]> opCodesToIgnore)
        {
            IgnoredOpCodes.OpCodes = opCodesToIgnore;
            IgnoredOpCodes.StartByte = opCodeStartByte;
        }

        private void downloadProgressReport(int percents)
        {
            if (OnDownloadProgressReport != null)
                OnDownloadProgressReport.BeginInvoke(this, percents, null, null);
        }

        public virtual string DeviceName
        {
            get { return deviceIO.ShortId; }
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                if (loop != null)
                    loop.Dispose();

                unregisterIOEvents();
                OnDownloadProgressReport = null;
                OnDownloading = null;
                ErrorStatus = null;

                deviceIO.Dispose();
            }
        }

        #endregion
    }
}
