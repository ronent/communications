﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auxiliary.Tools;

namespace WCFClient.Devices.Optimizations
{
    public class IgnoredOpCodes
    {
        public IEnumerable<byte[]> OpCodes = null;
        public int StartByte { get; set; }

        public bool Contains(byte[] data)
        {
            if (OpCodes != null)
                foreach (var opcode in OpCodes)
                    if (opcode.SequenceEqual(data.SubArray(StartByte, opcode.Length)))
                        return true;

            return false;
        }
    }
}
