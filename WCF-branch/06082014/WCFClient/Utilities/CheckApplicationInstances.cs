﻿using HidLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WCFClient.Utilities
{
    public delegate void InstanceChangedDelegate(bool isRunning);

    public class CheckApplicationInstances : IDisposable
    {
        #region Fields
        public InstanceChangedDelegate OnInstanceChanged;

        private const int k_TimeToSleep = 200;
        private string appName;
        private volatile bool m_IsDataSuiteRunning;

        #endregion
        #region Constructor
        public CheckApplicationInstances(string appName)
        {
            this.appName = appName;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        public bool IsDataSuiteRunning
        {
            get { return m_IsDataSuiteRunning; }
            private set
            {
                if (m_IsDataSuiteRunning != value)
                {
                    m_IsDataSuiteRunning = value;

                    try
                    {
                        if (OnInstanceChanged != null)
                            OnInstanceChanged(value);
                    }
                    catch { }
                }
            }
        }

        #endregion
        #region Methods
        public void Start()
        {
            Task.Factory.StartNew(() =>
            {
                while (!IsDisposed)
                {
                    try
                    {
                        Process[] dataSuiteProcess = Process.GetProcessesByName(appName);
                        IsDataSuiteRunning = dataSuiteProcess.Length != 0;

                        TaskDelay.Delay(k_TimeToSleep).Wait();
                    }
                    catch (Exception ex)
                    {
                        Debugger.Break();
                        var a = ex.Message;
                    }
                }

            }, TaskCreationOptions.LongRunning);
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                OnInstanceChanged = null;
            }
        }

        #endregion
    }
}