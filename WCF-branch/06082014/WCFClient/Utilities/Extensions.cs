﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFClient.Utilities
{
    public static class Extensions
    {
        public static string ToShortId(this string id)
        {
            return id.Split('&')[2];
        }
    }
}
