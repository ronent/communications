﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFClient.Utilities
{
    public enum eTasks
    {
        DownloadData,
        DownloadTS,
        GenerateReport,
        DownloadReport,
    }

    public class PartialProgress
    {
        public event ProgressReportDelegate OnProgressReport;

        public eTasks Task { get; private set; }
        public int Start { get; private set; }
        public int End { get; private set; }
        public float Proportion { get { return (End - Start) / 100f; } }
        public bool IsFinished { get; private set; }

        public PartialProgress(eTasks task, int start, int end)
        {
            Task = task;
            Start = start;
            End = end;
            IsFinished = false;
        }

        public void UpdatePercent(int percent)
        {
            if (percent == 100)
                IsFinished = true;

            int partialPercent = Math.Min(Start + Convert.ToInt16(Proportion * percent), End);
            invokeOnProgressReport(partialPercent);
        }

        private void invokeOnProgressReport(int percent)
        {
            if (OnProgressReport != null)
                OnProgressReport.Invoke(this, percent);
        }
    }
}
