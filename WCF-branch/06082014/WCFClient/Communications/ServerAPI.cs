﻿using HidLibrary;
using Infrastructure.Communication;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WCFClient.Service;
using WCFClient.ServiceReference;
using WCFClient.Utilities;

namespace WCFClient.Communications
{
    public delegate void ConnectionStateChangedDelegate(CommunicationState state);

    public class ServerAPI : IDisposable
    {
        #region Members
        public event ConnectionStateChangedDelegate OnConnectionStateChanged;

        protected CommunicationManager parent { get; set; }
        protected ServiceManager service { get; set; }

        private KeepAlive keepAlive;

        private volatile int reconnectRetries = RECONNECT_RETRIES;

        private static bool useRetry = true;
        private static int RECONNECT_RETRIES = 5;
        private static TimeSpan TIME_OUT = new TimeSpan(0, 0, 30);
        private static TimeSpan RETRY = new TimeSpan(0, 0, 5);

        private bool keepAliveLogEnabled = true;
        private CommunicationState? lastState;

        #endregion
        #region Constructor
        public ServerAPI(CommunicationManager communicationManager)
        {
            IsDisposed = false;
            parent = communicationManager;

            keepAlive = new KeepAlive(this);
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        public System.ServiceModel.CommunicationState State
        {
            get
            {
                if (lastState != null)
                {
                    return (CommunicationState)lastState;
                }

                return CommunicationState.Closed;
            }
        }

        public DateTime LastServerCommunication { get; private set; }

        public bool IsConnected
        {
            get
            {
                if (service == null)
                    return false;
                else
                    return service.State == System.ServiceModel.CommunicationState.Opened;
            }
        }

        public bool IsConnecting
        {
            get
            {
                if (service == null)
                    return false;
                else
                    return service.State == System.ServiceModel.CommunicationState.Opening;
            }
        }

        protected ClientAPI client { get { return parent.client; } }
        private DeviceManager deviceManager { get { return parent.deviceManager; } }

        #endregion
        #region Methods
        public void Connect()
        {
            try
            {
                if (IsConnected || IsConnecting)
                    return;

                Log.Instance.Write("[WCF Client] ==> Connecting [#{0} retry] to {1}.", this, Log.LogSeverity.DEBUG, RECONNECT_RETRIES - reconnectRetries + 1, parent.ServerURI);
                initializeServer();

                service.Open();
                service.Faulted += instanceContext_Faulted;

                handleConnected();
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("No DNS entries exist for host") && !ex.Message.Contains("TCP error code 10061") &&
                    !ex.Message.Contains("The open operation did not complete"))
                {
                    Log.Instance.WriteError("[WCF Client] ==> On Connect()", this, ex);
                }
                else
                    Log.Instance.Write("[WCF Client] ==> Failed to establish connection.", this);

                handleDisconnection(true);
            }
        }

        protected virtual void initializeServer()
        {
            service = new ServiceManager(parent.ServerURI ,client);
        }

        private void handleConnected()
        {
            try
            {
                Log.Instance.Write("[WCF Client] ==> Connected.", this);

                service.OpenSession();

                reconnectRetries = RECONNECT_RETRIES;
                updateLastServerCommunication();
                keepAlive.Start();

                invokeConnectionStateChanged(CommunicationState.Opened);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[WCF Client] ==> On handleConnected()", this, ex);
            }
        }

        public void Disconnect()
        {
            try
            {
                handleDisconnection(false);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[WCF Client] ==> On Disconnect()", this, ex);
            }
        }

        private void handleDisconnection(bool tryReconnect = true)
        {
            if (service != null)
                service.Close();

            if (keepAlive != null)
                keepAlive.Stop();

            if (tryReconnect)
            {
                invokeConnectionStateChanged(CommunicationState.Opening);
                reconnect();
            }
            else
            {
                Log.Instance.Write("[WCF Client] ==> Disconnected.", this);
                invokeConnectionStateChanged(CommunicationState.Closed);
            }
        }

        private void reconnect()
        {
            if (reconnectRetries == 1)
            {
                Log.Instance.Write("[WCF Client] ==> TIME OUT", this);
                TaskDelay.Delay(TIME_OUT).Wait();
                reconnectRetries = RECONNECT_RETRIES;
            }
            else
            {
                if (reconnectRetries > 0)
                {
                    TaskDelay.Delay(RETRY).Wait();

                    if (useRetry)
                        reconnectRetries--;
                }
                else
                {
                    reconnectRetries = RECONNECT_RETRIES;
                }
            }

            Connect();
        }

        private void instanceContext_Faulted(object sender, System.EventArgs e)
        {
            handleDisconnection(true);            
        }

        private void updateLastServerCommunication()
        {
            LastServerCommunication = DateTime.Now;
        }

        private void invokeConnectionStateChanged(CommunicationState state)
        {
            if (lastState == null)
            {
                lastState = state;

                if (OnConnectionStateChanged != null)
                    OnConnectionStateChanged(state);
            }
            else
            {
                if (lastState != state)
                {
                    lastState = state;

                    if (OnConnectionStateChanged != null)
                        OnConnectionStateChanged(state);
                }
            }
        }

        #endregion
        #region IServer Methods
        public void OnDeviceConnected(ConnectionEventArgs args)
        {
            Log.Instance.Write("[WCF Client] ==> New device connected: '{0}'", this, Log.LogSeverity.DEBUG, args.DeviceIO.ShortId);

            service.OnDeviceConnected(args);
            updateLastServerCommunication();
        }

        public void OnDeviceRemoved(ConnectionEventArgs args)
        {
            Log.Instance.Write("[WCF Client] ==> Device disconnected: '{0}'", this, Log.LogSeverity.DEBUG, args.DeviceIO.ShortId);

            service.OnDeviceRemoved(args);
            updateLastServerCommunication();
        }

        public void SendData(string id, byte[] data)
        {
            service.OnDataReceived(id, data);
        }

        public void OnBundleReceived(string id, IEnumerable<byte[]> data)
        {
            service.OnBundleReceived(id, data.ToArray());
            updateLastServerCommunication();
        }

        public void KeepAlive()
        {
            service.KeepAlive();
            updateLastServerCommunication();
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                if (service != null)
                    service.Dispose();

                if(keepAlive != null)
                    keepAlive.Dispose();

                parent = null;
                service = null;
                keepAlive = null;

                OnConnectionStateChanged = null;
            }
        }

        #endregion
    }
}