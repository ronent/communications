﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WCFClient.Devices;

namespace WCFClient.Communications.EventArgs
{
    public class DeviceConnectionEventArgs : System.EventArgs
    {
        public eDeviceConnecionState State { get; private set; }
        public DeviceIO DeviceIO { get; private set; }

        public DeviceConnectionEventArgs(eDeviceConnecionState state, DeviceIO device)
            :base()
        {
            State = state;
            DeviceIO = device;
        }
    }
}
