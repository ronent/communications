﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFClient.Communications.EventArgs
{
    public class StatusEventArgs : System.EventArgs
    {
        public string Status { get; set; }
        public bool IsError { get; set; }
    }
}
