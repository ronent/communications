﻿using System;
namespace WCFClient.Service
{
    public interface IServiceManager
    {
        void Close();
        event EventHandler Faulted;
        void Open();
        global::System.ServiceModel.CommunicationState State { get; }
    }
}
